!classDefinition: #I category: 'Natural Numbers'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Natural Numbers'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'as yet unclassified' stamp: 'MC 3/28/2023 20:47:43'!
* aNaturalNumber
	^aNaturalNumber.! !

!I class methodsFor: 'as yet unclassified' stamp: 'nO 3/27/2023 20:36:41'!
+ aNaturalNumber
	^aNaturalNumber next.! !

!I class methodsFor: 'as yet unclassified' stamp: 'MC 3/29/2023 20:42:31'!
/ aNaturalNumber

	(aNaturalNumber = self) ifTrue: [^ I].

	^ I + (self - aNaturalNumber / aNaturalNumber)! !

!I class methodsFor: 'as yet unclassified' stamp: 'nO 3/27/2023 20:03:46'!
next
	^II.! !

!I class methodsFor: 'as yet unclassified' stamp: 'MC 3/30/2023 03:12:38'!
subtractFrom: aNaturalNumber
	^aNaturalNumber previous.! !


!classDefinition: #II category: 'Natural Numbers'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Natural Numbers'!
II class
	instanceVariableNames: 'next prev'!

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/30/2023 03:12:17'!
* aNaturalNumber

	^aNaturalNumber + (self previous * aNaturalNumber)
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/30/2023 03:12:12'!
+ aNaturalNumber

	^(self previous) + (aNaturalNumber next)
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/29/2023 19:47:25'!
- aNaturalNumber

	^aNaturalNumber subtractFrom: self
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/29/2023 20:41:32'!
/ aNaturalNumber

	(aNaturalNumber = self) ifTrue: [^ I].

	^ I + (self - aNaturalNumber / aNaturalNumber)
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/30/2023 21:39:04'!
next

	next ifNil: [next := II createChildNamed: self name, 'I'.
		next previous: self].
	^next
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/30/2023 03:11:27'!
previous

	^prev
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'nO 3/27/2023 21:22:02'!
previous: valorPrevio

	prev := valorPrevio.
	
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'MC 3/30/2023 03:11:52'!
subtractFrom: aNaturalNumber
	
	^aNaturalNumber previous - self previous.
	
	! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'MC 4/2/2023 20:31:53'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	prev := I.! !


!classDefinition: #III category: 'Natural Numbers'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'Natural Numbers'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'MC 4/2/2023 20:31:53'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	prev := II.! !


!classDefinition: #IIII category: 'Natural Numbers'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Natural Numbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: 'Natural Numbers'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: '--** private fileout/in **--' stamp: 'MC 4/2/2023 20:31:53'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	prev := III.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!