!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'mc 6/26/2023 20:51:55'!
test01CanImportValidImportData

	CustomerImporter valueFrom: self validImportData into: session.

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 16:18:29'!
test02RaisesErrorIfDataHasNoCustomerBeforeAnAddress

	self should: [CustomerImporter valueFrom: self dataWithAddressButNoCustomer into: session.
				self fail]
		raise: Error
		withMessageText: [CustomerImporter cannotImportAddressWithoutCustomerErrorDescription]! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:02:09'!
test03RaisesErrorIfLineStartsWithInvalidRecordType

	self should: [CustomerImporter valueFrom: self dataWithIncorrectRecordType into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError | 
			self assert: anError messageText = CustomerImporter missingValidRecordTypeErrorDescription.
			self assertSessionHasNoCustomers]
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:02:09'!
test04RaisesErrorIfLineInCustomerDataIsMissingFields

	self should: [CustomerImporter valueFrom: self dataWithCustomerLineWithMissingFields into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError | 
			self assert: anError messageText = CustomerImporter incorrectAmountOfFieldsErrorDescription.
			self assertSessionHasNoCustomers]
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:02:09'!
test05RaisesErrorIfLineInCustomerDataHasAdditionalFields

	self should: [CustomerImporter valueFrom: self dataWithCustomerLineWithAdditionalFields into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError | 
			self assert: anError messageText = CustomerImporter incorrectAmountOfFieldsErrorDescription.
			self assertSessionHasNoCustomers]
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:10:14'!
test06RaisesErrorIfLineInAddressDataIsMissingFields

	self should: [CustomerImporter valueFrom: self dataWithAddressLineWithMissingFields into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError |  
			self assert: anError messageText = CustomerImporter incorrectAmountOfFieldsErrorDescription.
			self assertSessionHasACustomerWithNoAddresses]
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:10:19'!
test07RaisesErrorIfLineInAddressDataHasAdditionalFields

	self should: [CustomerImporter valueFrom: self dataWithAddressLineWithAdditionalFields into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError |  
			self assert: anError messageText = CustomerImporter incorrectAmountOfFieldsErrorDescription.
			self assertSessionHasACustomerWithNoAddresses]
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:03:10'!
test08RaisesErrorIfStreetNumberIsNotANumber

	self should: [CustomerImporter valueFrom: self dataWithInvalidStreetNumber into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError | self assertSessionHasACustomerWithNoAddresses]
		
	
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:03:10'!
test09RaisesErrorIfZipCodeIsNotANumber

	self should: [CustomerImporter valueFrom: self dataWithInvalidZipCode into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError | self assertSessionHasACustomerWithNoAddresses]
		
	
! !

!ImportTest methodsFor: 'tests' stamp: 'mc 6/27/2023 17:10:25'!
test10RaisesErrorIfALineHasNoFields

	self should: [CustomerImporter valueFrom: self validImportDataWithAnEmptyLineInTheMiddle into: session.
				self fail]
		raise: Error
		withExceptionDo: [:anError |   
			self assert: anError messageText = CustomerImporter missingValidRecordTypeErrorDescription.
			self assertSessionHasACustomerWithNoAddresses]
	
		
	
! !


!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:27:57'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := self customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:12:18'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !

!ImportTest methodsFor: 'assertions' stamp: 'mc 6/27/2023 17:17:50'!
assertSessionHasACustomerWithNoAddresses

	| customer | 
	
	customer := self customerWithIdentificationType: 'D' number: '22333444'.
	self assert: 0 equals: customer amountOfAddresses! !

!ImportTest methodsFor: 'assertions' stamp: 'mc 6/27/2023 17:17:44'!
assertSessionHasNoCustomers

	self assert: 0 equals: (session selectAllOfType: Customer) size! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:27:50'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !


!ImportTest methodsFor: 'customer' stamp: 'HAW 5/22/2022 18:14:22'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ (session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !


!ImportTest methodsFor: 'test data' stamp: 'mc 6/26/2023 21:10:17'!
dataWithAddressButNoCustomer

	^ ReadStream on: 
'A,San Martin,3322,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:11:41'!
dataWithAddressLineWithAdditionalFields

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Canada,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:09:15'!
dataWithAddressLineWithMissingFields

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:21:25'!
dataWithCustomerLineWithAdditionalFields

	^ ReadStream on:
'C,Pepe,"Pepito",Sanchez,D,22333444'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:21:19'!
dataWithCustomerLineWithMissingFields

	^ ReadStream on:
'C,Pepe,Sanchez'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:19:42'!
dataWithIncorrectRecordType

	^ ReadStream on:
'c,Pepe,Sanchez,D,22333444'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/26/2023 21:49:12'!
dataWithInvalidStreetNumber

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,street number,Olivos,1636,BsAs'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:05:31'!
dataWithInvalidZipCode

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,zip code,BsAs'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:37:56'!
emptyData

	^ ReadStream on: 
''! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/26/2023 21:10:04'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'mc 6/27/2023 16:56:04'!
validImportDataWithAnEmptyLineInTheMiddle

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444

A,San Martin,3322,Olivos,1636,BsAs'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !

!Customer methodsFor: 'addresses' stamp: 'mc 6/27/2023 16:03:31'!
amountOfAddresses

	^ addresses size! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream line newCustomer record'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'HAW 5/22/2022 18:06:47'!
initializeFrom: aReadStream into: aSession
	session := aSession.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'assertions' stamp: 'mc 6/26/2023 21:36:57'!
assertAmountOfFieldsInRecordIs: anAmountOfFields

	^ (record size = anAmountOfFields) ifFalse: [self error: self class incorrectAmountOfFieldsErrorDescription ]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'mc 6/26/2023 21:02:34'!
assertCustomerHasBeenImported

	self noCustomerHasBeenImported ifTrue: [self error: self class cannotImportAddressWithoutCustomerErrorDescription]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'mc 6/27/2023 16:57:59'!
assertRecordTypeIsValid

	^ self recordTypeIsValid ifFalse: [self error: self class missingValidRecordTypeErrorDescription]! !


!CustomerImporter methodsFor: 'testing' stamp: 'mc 6/26/2023 20:22:12'!
hasLinesLeft

	line := readStream nextLine. 
	^ line notNil! !

!CustomerImporter methodsFor: 'testing' stamp: 'mc 6/26/2023 20:12:50'!
isAddressRecord

	^ line beginsWith: 'A'! !

!CustomerImporter methodsFor: 'testing' stamp: 'mc 6/26/2023 20:12:20'!
isCustomerRecord

	^ line beginsWith: 'C'! !

!CustomerImporter methodsFor: 'testing' stamp: 'mc 6/26/2023 21:02:21'!
noCustomerHasBeenImported

	^ newCustomer isNil! !

!CustomerImporter methodsFor: 'testing' stamp: 'mc 6/26/2023 21:16:52'!
recordTypeIsValid

	^ self isAddressRecord | self isCustomerRecord ! !


!CustomerImporter methodsFor: 'importing' stamp: 'mc 6/27/2023 16:09:32'!
importAddress

	| newAddress |
	
	self assertAmountOfFieldsInRecordIs: 6.
	
	newAddress := Address new.
	newAddress streetName: record second.
	newAddress streetNumber: record third asNumber .
	newAddress town: record fourth.
	newAddress zipCode: record fifth asNumber .
	newAddress province: record sixth.
	newCustomer addAddress: newAddress.! !

!CustomerImporter methodsFor: 'importing' stamp: 'mc 6/26/2023 21:36:41'!
importCustomer

	self assertAmountOfFieldsInRecordIs: 5.
	
	newCustomer := Customer new.
	newCustomer firstName: record second.
	newCustomer lastName: record third.
	newCustomer identificationType: record fourth.
	newCustomer identificationNumber: record fifth.
	session persist: newCustomer! !

!CustomerImporter methodsFor: 'importing' stamp: 'mc 6/26/2023 21:16:07'!
importRecord

	self assertRecordTypeIsValid.
	
	self isCustomerRecord ifTrue: [ self importCustomer ].
	self isAddressRecord ifTrue: [ self assertCustomerHasBeenImported.
							self importAddress ]
	! !


!CustomerImporter methodsFor: 'record' stamp: 'mc 6/26/2023 20:14:59'!
createRecord

	^ record := line findTokens: $,! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'mc 6/26/2023 20:22:36'!
value

	[self hasLinesLeft ] whileTrue: [
		self createRecord.
		self importRecord]

	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'importing' stamp: 'HAW 5/22/2022 18:11:27'!
valueFrom: aReadStream into: aSession

	^(self from: aReadStream into: aSession) value! !


!CustomerImporter class methodsFor: 'error descriptions' stamp: 'mc 6/26/2023 20:49:54'!
cannotImportAddressWithoutCustomerErrorDescription

	^ 'Cannot import an address without importing a customer first'! !

!CustomerImporter class methodsFor: 'error descriptions' stamp: 'mc 6/26/2023 21:50:35'!
incorrectAmountOfFieldsErrorDescription

	^ 'Line in stream has incorrect amount of fields'! !

!CustomerImporter class methodsFor: 'error descriptions' stamp: 'mc 6/27/2023 16:57:59'!
missingValidRecordTypeErrorDescription

	^ 'Each line must begin with a record type of value C or A'! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
