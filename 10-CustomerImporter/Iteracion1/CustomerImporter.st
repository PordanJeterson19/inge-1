!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'test' stamp: 'mc 6/25/2023 18:33:11'!
test01ImportFromDefaultStreamIsSuccessful
	
	self importCustomersFrom: self defaultInputStream.
	
	self assert: 2 equals: self selectAllCustomers size. 
	
	self assertCustomerPepeWasImportedCorrectly.
	
	self assertCustomerJuanWasImportedCorrectly .
	
	
	
	
	! !


!ImportTest methodsFor: 'assertions - pepe' stamp: 'das 6/23/2023 16:48:16'!
assertCustomerPepeWasImportedCorrectly

	| collectionWithCustomerPepe customerPepe |
	
	collectionWithCustomerPepe := self selectCustomersWithDNI22333444.

	self assert: 1 equals: collectionWithCustomerPepe size.

	customerPepe := collectionWithCustomerPepe anyOne.

	self assertCustomerPepesDataWasImportedCorrectly: customerPepe.
	
	self assertCustomerPepesAddressesWereImportedCorrectly: customerPepe! !

!ImportTest methodsFor: 'assertions - pepe' stamp: 'mc 6/24/2023 17:55:20'!
assertCustomerPepesAddressesWereImportedCorrectly: customerPepe

	self assertCustomer: customerPepe wasImportedWithAddresses: {self pepeAddress1. self pepeAddress2}! !

!ImportTest methodsFor: 'assertions - pepe' stamp: 'mc 6/24/2023 17:34:23'!
assertCustomerPepesDataWasImportedCorrectly: customerPepe

	self assertCustomer: customerPepe
		wasImportedWithFirstName: 'Pepe' 
		lastName: 'Sanchez' 
		idType: 'D' 
		andIdNumber: '22333444'! !


!ImportTest methodsFor: 'assertions - juan' stamp: 'das 6/23/2023 16:47:32'!
assertCustomerJuanWasImportedCorrectly
	
	| collectionWithCustomerJuan customerJuan |
	
	collectionWithCustomerJuan := self selectCustomersWithCuit23256667779.
											
	self assert: 1 equals: collectionWithCustomerJuan size.
	
	customerJuan := collectionWithCustomerJuan anyOne.
	
	self assertCustomerJuansDataWasImportedCorrectly: customerJuan.
	
	self assertCustomerJuansAddressesWereImportedCorrectly: customerJuan.
	
	
	
	
	! !

!ImportTest methodsFor: 'assertions - juan' stamp: 'mc 6/24/2023 17:55:26'!
assertCustomerJuansAddressesWereImportedCorrectly: customerJuan

	self assertCustomer: customerJuan wasImportedWithAddresses: {self juanAddress}! !

!ImportTest methodsFor: 'assertions - juan' stamp: 'mc 6/24/2023 17:34:18'!
assertCustomerJuansDataWasImportedCorrectly: customerJuan

	self assertCustomer: customerJuan 
		wasImportedWithFirstName: 'Juan' 
		lastName: 'Perez' 
		idType: 'C' 
		andIdNumber: '23-25666777-9'! !


!ImportTest methodsFor: 'assertions' stamp: 'mc 6/24/2023 17:48:28'!
assertCustomer: aCustomer wasImportedWithAddresses: anArrayOfAddresses

	self assert: anArrayOfAddresses size equals: aCustomer amountOfAddresses.
	
	anArrayOfAddresses do: [:address | self assert: (aCustomer hasAddress: address)].! !

!ImportTest methodsFor: 'assertions' stamp: 'mc 6/24/2023 17:30:40'!
assertCustomer: aCustomer wasImportedWithFirstName: aFirstName lastName: aLastName idType: anIdType andIdNumber: idNumber

	self assert: (aCustomer hasFirstName: aFirstName andLastName: aLastName).
	self assert: (aCustomer hasIdType: anIdType andIdNumber: idNumber)! !


!ImportTest methodsFor: 'addresses' stamp: 'das 6/23/2023 16:52:49'!
addressWith: aStreetName with: aStreetNumber with: aTown with: aZipCode with: aProvince
	
	| address |
	address := Address new.
	address streetName: aStreetName.
	address streetNumber: aStreetNumber.
	address town: aTown.
	address zipCode: aZipCode.
	address province: aProvince.
	
	^address
	

	
	
	
	! !

!ImportTest methodsFor: 'addresses' stamp: 'das 6/23/2023 16:55:32'!
juanAddress
	
	^self addressWith: 'Alem' with: 1122 with: 'CABA' with: 1001 with: 'CABA'.


	
	
	
	! !

!ImportTest methodsFor: 'addresses' stamp: 'das 6/23/2023 16:54:11'!
pepeAddress1

	^self addressWith: 'San Martin' with: 3322 with: 'Olivos' with: 1636 with: 'BsAs'.

	
	
	
	
	
	! !

!ImportTest methodsFor: 'addresses' stamp: 'das 6/23/2023 16:54:50'!
pepeAddress2
	
	^self addressWith: 'Maipu' with: 888 with: 'Florida' with: 1122 with: 'Buenos Aires'.
	
	
	
	
	
	
	! !


!ImportTest methodsFor: 'setUp' stamp: 'das 6/23/2023 16:29:48'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction! !


!ImportTest methodsFor: 'tearDown' stamp: 'das 6/23/2023 16:29:48'!
tearDown

	session commit.
	session close! !


!ImportTest methodsFor: 'streams' stamp: 'mc 6/24/2023 17:40:26'!
defaultInputStream

	| string |
	string := 'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA
'.

	^ReadStream on: string from: 0 to: string size.
	! !


!ImportTest methodsFor: 'import' stamp: 'mc 6/25/2023 18:39:02'!
importCustomersFrom: anInputReadStream

	(CustomerImporter from: anInputReadStream into: session) importCustomers ! !


!ImportTest methodsFor: 'customer selection' stamp: 'das 6/23/2023 16:31:20'!
selectAllCustomers

	^ session selectAllOfType: Customer! !

!ImportTest methodsFor: 'customer selection' stamp: 'mc 6/24/2023 17:38:30'!
selectCustomersWithCuit23256667779

	^ self selectCustomersWithIdType: 'C' number: '23-25666777-9'! !

!ImportTest methodsFor: 'customer selection' stamp: 'mc 6/24/2023 17:38:55'!
selectCustomersWithDNI22333444

	^ self selectCustomersWithIdType: 'D' number: '22333444'! !

!ImportTest methodsFor: 'customer selection' stamp: 'mc 6/24/2023 17:37:27'!
selectCustomersWithIdType: idType number: idNumber

	^ session select: [:customer | customer hasIdType: idType andIdNumber: idNumber] ofType: Customer! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'town' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'town' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!Address methodsFor: 'comparing' stamp: 'mc 6/24/2023 18:01:41'!
isAddress: anAddress

	^ (streetName = anAddress streetName) &
		(streetNumber = anAddress streetNumber) &
		(town = anAddress town) &
		(zipCode = anAddress zipCode) &
		(province = anAddress province)
	! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !

!Customer methodsFor: 'addresses' stamp: 'mc 6/22/2023 21:48:00'!
amountOfAddresses

	^ addresses size! !

!Customer methodsFor: 'addresses' stamp: 'mc 6/23/2023 06:27:19'!
hasAddress: anAddress

	^ addresses anySatisfy: [:address | address isAddress: anAddress]! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'mc 6/22/2023 21:22:12'!
hasFirstName: firstNameString andLastName: lastNameString

	^ firstName = firstNameString and: lastName = lastNameString ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'mc 6/22/2023 21:43:27'!
hasIdType: idTypeCharacter andIdNumber: idNumber

	^ identificationType = idTypeCharacter and: identificationNumber = idNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session stream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'mc 6/24/2023 17:42:41'!
initializeFrom: anAnInputReadStream into: aSession
	session := aSession.
	stream := anAnInputReadStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'mc 6/25/2023 18:33:21'!
importCustomers

	| newCustomer line | 

	line := stream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth.
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth ].

		line := stream nextLine. ].

	stream close.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'das 6/23/2023 17:48:08'!
from: anAnInputReadStream into: aSession
	^self new initializeFrom: anAnInputReadStream into: aSession! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
