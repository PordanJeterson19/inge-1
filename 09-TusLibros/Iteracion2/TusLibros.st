!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'support'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'mc 6/8/2023 19:55:59'!
test01NewCartsAreCreatedEmpty

	self assert: support createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:19'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := support createCart.
	
	self 
		should: [ cart add: support itemNotSoldByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:29'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := support createCart.
	
	cart add: support itemSoldByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:29'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := support createCart.
	
	self 
		should: [cart add: 0 of: support itemSoldByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:19'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := support createCart.
	
	self 
		should: [cart add: 2 of: support itemNotSoldByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:29'!
test06CartRemembersAddedItems

	| cart |
	
	cart := support createCart.
	
	cart add: support itemSoldByTheStore.
	self assert: (cart includes: support itemSoldByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:29'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := support createCart.
	
	self deny: (cart includes: support itemSoldByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/9/2023 04:33:29'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := support createCart.
	
	cart add: 2 of: support itemSoldByTheStore.
	self assert: (cart occurrencesOf: support itemSoldByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'mc 6/8/2023 19:55:37'!
setUp

	support := TestingSupport new

	! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'support cashier salesBook cashierCurrentDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'setup' stamp: 'mc 6/10/2023 16:14:40'!
setUp

	support := TestingSupport new.
	
	cashier := Cashier new.
	
	salesBook := OrderedCollection new.
	
	cashierCurrentDate := support june2023.

	! !


!CashierTest methodsFor: 'assertions' stamp: 'mc 6/11/2023 22:13:36'!
assertCheckoutOf: aCart using: aCreditCard with: aMerchantProcessor makesSaleForTotalOf: aTotalAmount

	
	self assert: aTotalAmount equals: (cashier checkout: aCart 
								withCard: aCreditCard 
								withMerchantProcessor: aMerchantProcessor 
								onDate: cashierCurrentDate
								andLogInto: salesBook).
	self assert: aTotalAmount equals: salesBook first.


	! !

!CashierTest methodsFor: 'assertions' stamp: 'mc 6/11/2023 21:58:12'!
assertCheckoutOf: aCart using: aCreditCard with: aMerchantProcessor raisesError: errorString


	self 
		should: [cashier checkout: aCart
						withCard: aCreditCard 
						withMerchantProcessor: aMerchantProcessor 
						onDate: cashierCurrentDate 
						andLogInto: salesBook.
				self fail.]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = errorString.
			self assert: salesBook isEmpty]


	! !


!CashierTest methodsFor: 'tests' stamp: 'mc 6/11/2023 22:17:09'!
test01EmptyCartCheckoutRaisesError

	| cart creditCard merchantProcessor |
	
	cart := support createCart.
	
	creditCard := support creditCardExpiringJuly2023.
	
	merchantProcessor := MerchantProcessorForValidCards new.
	
	self assertCheckoutOf: cart 
		using: creditCard 
		with: merchantProcessor 
		raisesError: Cashier emptyCartErrorDescription

	! !

!CashierTest methodsFor: 'tests' stamp: 'mc 6/11/2023 22:20:23'!
test02CheckoutOfCartWithOneProductReturnsTotal

	| cart creditCard merchantProcessor |
	
	cart := self cartWithBookOfPrice10.
	
	creditCard := support creditCardExpiringJuly2023.
	
	merchantProcessor := MerchantProcessorForValidCards new.
	
	self assertCheckoutOf: cart 
		using: creditCard 
		with: merchantProcessor 
		makesSaleForTotalOf: 10

	! !

!CashierTest methodsFor: 'tests' stamp: 'mc 6/11/2023 22:20:58'!
test03CheckoutOfCartWithProductsReturnsTotal

	| cart creditCard merchantProcessor |
	
	cart := self cartWithTwoBooksOfPrices10And20.
	
	creditCard := support creditCardExpiringJuly2023.
	
	merchantProcessor := MerchantProcessorForValidCards new.
	
	self assertCheckoutOf: cart 
		using: creditCard 
		with: merchantProcessor 
		makesSaleForTotalOf: 30


	! !

!CashierTest methodsFor: 'tests' stamp: 'mc 6/11/2023 22:20:23'!
test04CheckoutWithExpiredCreditCardRaisesError

	| cart creditCard merchantProcessor |
	
	cart := self cartWithBookOfPrice10.
	
	creditCard := support creditCardExpiringMarch2023.
	
	merchantProcessor := MerchantProcessorForValidCards new.
	
	self assertCheckoutOf: cart 
		using: creditCard 
		with: merchantProcessor 
		raisesError: Cashier expiredCardErrorDescription

	! !

!CashierTest methodsFor: 'tests' stamp: 'mc 6/11/2023 22:20:23'!
test05CheckoutWithCreditCardWithInsufficientFundsRaisesError

	| cart creditCard merchantProcessor |
	
	cart := self cartWithBookOfPrice10.
	
	creditCard := support creditCardExpiringJuly2023.
	
	merchantProcessor := MerchantProcessorForShortOnFundsCards new.
	
	self assertCheckoutOf: cart 
		using: creditCard 
		with: merchantProcessor 
		raisesError: MerchantProcessorForShortOnFundsCards insufficientFundsErrorDescription 


	! !

!CashierTest methodsFor: 'tests' stamp: 'mc 6/11/2023 22:20:23'!
test06CheckoutWithStolenCreditCardRaisesError

	| cart creditCard merchantProcessor |
	
	cart := self cartWithBookOfPrice10.
	
	creditCard := support creditCardExpiringJuly2023.
	
	merchantProcessor := MerchantProcessorForStolenCards new.
	
	self assertCheckoutOf: cart 
		using: creditCard 
		with: merchantProcessor 
		raisesError: MerchantProcessorForStolenCards stolenCardErrorDescription


	! !


!CashierTest methodsFor: 'carts' stamp: 'mc 6/11/2023 22:20:23'!
cartWithBookOfPrice10

	| cart |
	cart := support createCart.
	
	cart add: 'BookWithPriceOf10'.
	
	^cart! !

!CashierTest methodsFor: 'carts' stamp: 'mc 6/11/2023 22:20:58'!
cartWithTwoBooksOfPrices10And20

	| cart |
	cart := self cartWithBookOfPrice10. 
	
	cart add: 'BookWithPriceOf20'.
	
	^cart! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: 'support'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'setup' stamp: 'mc 6/9/2023 16:24:15'!
setUp

	support := TestingSupport new
! !


!CreditCardTest methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:43:55'!
creatingCardWithName: aName andNumber: aCardNumber raisesError: errorString

	self 
		should: [ CreditCard for: aName with: aCardNumber with: support march2023.
				self fail.]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = errorString]! !


!CreditCardTest methodsFor: 'tests' stamp: 'mc 6/10/2023 16:44:43'!
test01CreditCardRaisesErrorIfNameIsEmpty

	self creatingCardWithName: '' 
		andNumber: 1111111111111111
		raisesError: CreditCard invalidNameErrorDescription! !

!CreditCardTest methodsFor: 'tests' stamp: 'mc 6/10/2023 16:45:10'!
test02CreditCardRaisesErrorIfNumberIsLessThan16Digits

	self creatingCardWithName: 'pepito'
		andNumber: 111
		raisesError: CreditCard invalidNumberErrorDescription! !

!CreditCardTest methodsFor: 'tests' stamp: 'mc 6/10/2023 16:45:29'!
test03CreditCardRaisesErrorIfNumberIsMoreThan16Digits

	self creatingCardWithName: 'pepito'
		andNumber: 111111111111111111111111111
		raisesError: CreditCard invalidNumberErrorDescription! !


!classDefinition: #TestingSupport category: 'TusLibros'!
TestCase subclass: #TestingSupport
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TestingSupport methodsFor: 'defaultDates' stamp: 'mc 6/9/2023 16:13:56'!
july2023

	^GregorianMonthOfYear julyOf: (GregorianYear number: 2023).! !

!TestingSupport methodsFor: 'defaultDates' stamp: 'mc 6/9/2023 16:13:37'!
june2023

	^GregorianMonthOfYear juneOf: (GregorianYear number: 2023).! !

!TestingSupport methodsFor: 'defaultDates' stamp: 'mc 6/9/2023 16:13:46'!
march2023

	^GregorianMonthOfYear marchOf: (GregorianYear number: 2023).! !


!TestingSupport methodsFor: 'creditCards' stamp: 'mc 6/11/2023 22:17:09'!
creditCardExpiringJuly2023

	^CreditCard for: 'pepito' 
				with: 1111111111111111 
				with: (GregorianMonthOfYear julyOf: (GregorianYear number: 2023)).! !

!TestingSupport methodsFor: 'creditCards' stamp: 'mc 6/11/2023 22:17:26'!
creditCardExpiringMarch2023

	^CreditCard for: 'pepito' 
				with: 1111111111111111 
				with: (GregorianMonthOfYear marchOf: (GregorianYear number: 2023)).! !


!TestingSupport methodsFor: 'carts' stamp: 'mc 6/8/2023 21:40:25'!
createCart
	
	^Cart acceptingItemsOf: self defaultPriceList! !


!TestingSupport methodsFor: 'price lists' stamp: 'mc 6/9/2023 04:31:28'!
defaultPriceList

	| dictionary |
	dictionary := Dictionary new.
	dictionary at: 'validBook' put: 0.
	dictionary at: 'BookWithPriceOf10' put: 10.
	dictionary at: 'BookWithPriceOf20' put: 20.
	^dictionary! !


!TestingSupport methodsFor: 'sold items' stamp: 'mc 6/9/2023 04:33:19'!
itemNotSoldByTheStore
	
	^'invalidBook'! !

!TestingSupport methodsFor: 'sold items' stamp: 'mc 6/9/2023 04:33:29'!
itemSoldByTheStore
	
	^ 'validBook'! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'priceList items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'mc 6/8/2023 21:29:30'!
invalidItemErrorMessage
	
	^'Item is not in price list'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'mc 6/8/2023 21:22:10'!
assertIsValidItem: anItem

	(priceList includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:26:41'!
assertIsValidQuantity: aQuantity

	(self isAValidQuantity: aQuantity) ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'mc 6/9/2023 04:54:20'!
initializeAcceptingItemsOf: aPriceList

	priceList := aPriceList.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'mc 6/10/2023 16:26:20'!
isAValidQuantity: aQuantity

	^(aQuantity rounded = aQuantity) & (aQuantity strictlyPositive)! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'mc 6/9/2023 04:40:22'!
add: aQuantity of: anItem

	
	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	items add: anItem withOccurrences: aQuantity
	! !


!Cart methodsFor: 'total' stamp: 'mc 6/9/2023 04:55:20'!
total

	^ items sum: [:item | priceList at: item] ifEmpty: [0].
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:32:22'!
assert: aCart isNotEmptyAnd: aCreditCard isNotExpiredOn: aMonthOfYear 

	self assertCartIsNotEmpty: aCart.
	
	self asssert: aCreditCard isNotExpiredOn: aMonthOfYear 

	! !

!Cashier methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:31:29'!
assertCartIsNotEmpty: aCart 

	aCart isEmpty ifTrue: [self error: self class emptyCartErrorDescription].


	! !

!Cashier methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:32:06'!
asssert: aCreditCard isNotExpiredOn: aMonthOfYear 

	(aCreditCard isExpiredOn: aMonthOfYear) ifTrue: [self error: self class expiredCardErrorDescription].

	! !


!Cashier methodsFor: 'checkout' stamp: 'mc 6/10/2023 16:33:46'!
checkout: aCart withCard: aCreditCard withMerchantProcessor: aMerchantProcessorSimulator onDate: aMonthOfYear andLogInto: salesBook 

	| total |
	
	self assert: aCart isNotEmptyAnd: aCreditCard isNotExpiredOn: aMonthOfYear. 

	total := aCart total.
	
	aMerchantProcessorSimulator debit: total from: aCreditCard.
	
	^ salesBook addLast: total
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'error descriptions' stamp: 'mc 6/8/2023 20:05:02'!
emptyCartErrorDescription

	^'Cannot do checkout of empty cart'! !

!Cashier class methodsFor: 'error descriptions' stamp: 'mc 6/9/2023 16:03:00'!
expiredCardErrorDescription

	^'Cannot do checkout with an expired credit card'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'name expirationDate number'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'mc 6/9/2023 16:04:41'!
isExpiredOn: aMonthOfYear

	^ expirationDate <= aMonthOfYear! !


!CreditCard methodsFor: 'initialization' stamp: 'mc 6/9/2023 16:07:18'!
initializeFor: aName with: aCreditCardNumber with: aCardExpirationDate

	name := aName.
	number := aCreditCardNumber.
	expirationDate := aCardExpirationDate! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'mc 6/10/2023 16:36:54'!
for: aName with: aCreditCardNumber with: aCardExpirationDate

	self assertNameIsValid: aName andCardNumberIsValid: aCreditCardNumber .

	^self new initializeFor: aName with: aCreditCardNumber with: aCardExpirationDate! !


!CreditCard class methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:38:10'!
assertCardNumberIs16DigitsLong: aCreditCardNumber 

	(aCreditCardNumber asString size = 16) ifFalse: [self error: self invalidNumberErrorDescription].
! !

!CreditCard class methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:37:50'!
assertNameIsNotEmptyString: aName

	aName ifEmpty: [self error: self invalidNameErrorDescription].
	
! !

!CreditCard class methodsFor: 'assertions' stamp: 'mc 6/10/2023 16:38:34'!
assertNameIsValid: aName andCardNumberIsValid: aCreditCardNumber 

	self assertNameIsNotEmptyString: aName.
	
	self assertCardNumberIs16DigitsLong: aCreditCardNumber 
! !


!CreditCard class methodsFor: 'error descriptions' stamp: 'mc 6/9/2023 16:28:02'!
invalidNameErrorDescription

	^'Name cannot be empty'! !

!CreditCard class methodsFor: 'error descriptions' stamp: 'mc 6/9/2023 16:36:27'!
invalidNumberErrorDescription

	^'Credit card number must be 16 digits'! !


!classDefinition: #MerchantProcessorSimulator category: 'TusLibros'!
Object subclass: #MerchantProcessorSimulator
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorSimulator methodsFor: 'debit' stamp: 'mc 6/9/2023 17:31:15'!
debit: money from: aCreditCard

	self subclassResponsibility ! !


!classDefinition: #MerchantProcessorForShortOnFundsCards category: 'TusLibros'!
MerchantProcessorSimulator subclass: #MerchantProcessorForShortOnFundsCards
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorForShortOnFundsCards methodsFor: 'debit' stamp: 'mc 6/9/2023 17:31:25'!
debit: money from: aCreditCard

	self error: self class insufficientFundsErrorDescription  ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessorForShortOnFundsCards class' category: 'TusLibros'!
MerchantProcessorForShortOnFundsCards class
	instanceVariableNames: ''!

!MerchantProcessorForShortOnFundsCards class methodsFor: 'error descriptions' stamp: 'mc 6/9/2023 17:31:40'!
insufficientFundsErrorDescription 

	^ 'Credit card has insufficient funds for purchase'! !


!classDefinition: #MerchantProcessorForStolenCards category: 'TusLibros'!
MerchantProcessorSimulator subclass: #MerchantProcessorForStolenCards
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorForStolenCards methodsFor: 'debit' stamp: 'mc 6/9/2023 17:33:58'!
debit: money from: aCreditCard

	self error: self class stolenCardErrorDescription  ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessorForStolenCards class' category: 'TusLibros'!
MerchantProcessorForStolenCards class
	instanceVariableNames: ''!

!MerchantProcessorForStolenCards class methodsFor: 'error descriptions' stamp: 'mc 6/9/2023 17:34:26'!
stolenCardErrorDescription  

	^ 'This credit card is stolen'! !


!classDefinition: #MerchantProcessorForValidCards category: 'TusLibros'!
MerchantProcessorSimulator subclass: #MerchantProcessorForValidCards
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorForValidCards methodsFor: 'debit' stamp: 'mc 6/9/2023 17:39:03'!
debit: money from: aCreditCard

	^ self! !
