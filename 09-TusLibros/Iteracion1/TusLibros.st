!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'book1 book2'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'setup' stamp: 'mc 6/7/2023 05:11:07'!
setUp
	
	book1:= 1234.
	book2 := 5678.
	
	! !


!CartTest methodsFor: 'supermarkets' stamp: 'mc 6/7/2023 05:59:45'!
supermarketThatSellsBook1

	^Supermarket with: (Set with: book1).! !

!CartTest methodsFor: 'supermarkets' stamp: 'mc 6/7/2023 06:01:03'!
supermarketThatSellsBook1AndBook2

	^Supermarket with: (Set with: book1 with: book2).! !

!CartTest methodsFor: 'supermarkets' stamp: 'mc 6/7/2023 05:59:19'!
supermarketThatSellsNoBooks

	^Supermarket with: Set new.! !


!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:05:21'!
test01NewCartIsEmpty

	| cart |
	
	cart := Cart for: self supermarketThatSellsNoBooks. 
	
	self assert: cart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:08:33'!
test02NewCartListCartReturnsEmptyBag

	| cart |
	
	cart := Cart for: self supermarketThatSellsNoBooks.
	
	self assert: cart listCart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:09:04'!
test03AddingProductToNewCartMakesItNotEmpty

	| cart |
	
	cart := Cart for: self supermarketThatSellsBook1.
	
	cart add: book1 quantity:1.
	
	self deny: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:09:23'!
test04AddedProductToNewCartIsIncludedInItsContentWithRespectiveQuantity

	| cart tuple |
	
	cart := Cart for: self supermarketThatSellsBook1.
	
	cart add: book1 quantity:1.
	
	tuple := OrderedCollection with: book1 with: 1.
	
	self assert: cart listCart includes: tuple ! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:10:26'!
test05AddingProductNotContainedInSupermarketInventoryRaisesError

	| cart |
	
	cart := Cart for: self supermarketThatSellsNoBooks.
	
	self
        	should: [ cart add: book1 quantity:1.
         		self fail ]
        raise: Error 
        withExceptionDo: [ :anError |
            		self assert: anError messageText = Cart productIsNotInSupermarketInventoryErrorDescription.
		self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:15:20'!
test06ListCartReturnsContainedProductsWithTheirRespectiveQuantity

	| cart tupleBook1 tupleBook2 |
	
	cart := Cart for: self supermarketThatSellsBook1AndBook2.
	
	cart add: book1 quantity: 1.
	cart add: book2 quantity: 2.
	
	tupleBook1 := OrderedCollection with: book1 with:1.
	tupleBook2 := OrderedCollection with: book2 with:2.
	
	self assert: (Set with: tupleBook1 with: tupleBook2) equals: cart listCart! !

!CartTest methodsFor: 'tests' stamp: 'mc 6/7/2023 06:12:34'!
test07AddingProductWithAQuantityLowerThanOneRaisesError

	| cart |
	
	cart := Cart for: self supermarketThatSellsBook1.
	
	self
        	should: [cart add: book1 quantity: -1.
         		self fail ]
        raise: Error 
        withExceptionDo: [ :anError |
            		self assert: anError messageText = Cart quantityIsLowerThanOneErrorDescription.
		self assert: cart isEmpty ]
	
! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'contents supermarket'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'assertions' stamp: 'mc 6/7/2023 06:21:54'!
assertCanAdd: aProductISBN in: aQuantity

	self assertThatProductIsInInventory: aProductISBN.
	self assertThatQuantityIsNotLowerThanOne: aQuantity 
! !

!Cart methodsFor: 'assertions' stamp: 'asd 6/6/2023 21:31:44'!
assertThatProductIsInInventory: aProductISBN 

	(supermarket inventoryIncludes: aProductISBN) ifFalse: [^self error: self class productIsNotInSupermarketInventoryErrorDescription ].

! !

!Cart methodsFor: 'assertions' stamp: 'mc 6/7/2023 06:21:54'!
assertThatQuantityIsNotLowerThanOne: aQuantity

	(aQuantity >= 1) ifFalse: [^self error: self class quantityIsLowerThanOneErrorDescription ].
! !


!Cart methodsFor: 'testing' stamp: 'asd 6/6/2023 20:55:18'!
isEmpty

	^contents isEmpty! !


!Cart methodsFor: 'adding' stamp: 'asd 6/6/2023 21:33:40'!
add: aProductISBN quantity: aQuantity

	self assertCanAdd: aProductISBN in: aQuantity .

	contents add: aProductISBN withOccurrences: aQuantity.! !


!Cart methodsFor: 'content listing - auxiliary' stamp: 'asd 6/6/2023 21:35:26'!
tupleWithQuantityFor: product 

	^OrderedCollection with: product with: (contents occurrencesOf: product).
! !


!Cart methodsFor: 'content listing' stamp: 'asd 6/6/2023 21:35:59'!
listCart

	|cartList|
	
	cartList := Set new.
	
	contents asSet do: [:product| cartList add: (self tupleWithQuantityFor: product)].
				
	^cartList! !


!Cart methodsFor: 'initialization' stamp: 'asd 6/6/2023 20:46:31'!
initializeFor: aSupermarket

	supermarket := aSupermarket .
	contents := Bag new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'asd 6/6/2023 20:48:40'!
for: aSupermarket

	^self new initializeFor: aSupermarket ! !


!Cart class methodsFor: 'error description' stamp: 'asd 6/6/2023 21:26:12'!
productIsNotInSupermarketInventoryErrorDescription

	^'Product is not sold in supermarket'! !

!Cart class methodsFor: 'error description' stamp: 'asd 6/6/2023 21:25:52'!
quantityIsLowerThanOneErrorDescription

	^'Quantity has to be at least 1'! !


!classDefinition: #Supermarket category: 'TusLibros'!
Object subclass: #Supermarket
	instanceVariableNames: 'inventory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Supermarket methodsFor: 'inventory' stamp: 'asd 6/6/2023 21:04:57'!
inventoryIncludes: aProductISBN

	^inventory includes: aProductISBN ! !


!Supermarket methodsFor: 'initialization' stamp: 'asd 6/6/2023 21:09:04'!
initializeWith: anInventory

	inventory := anInventory ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Supermarket class' category: 'TusLibros'!
Supermarket class
	instanceVariableNames: 'inventory'!

!Supermarket class methodsFor: 'instance creation' stamp: 'asd 6/6/2023 21:08:42'!
with: anInventory

	^self new initializeWith: anInventory ! !
