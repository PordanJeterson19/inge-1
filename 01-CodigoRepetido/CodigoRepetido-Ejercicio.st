!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'MC 4/12/2023 00:29:57'!
checkThatThereAreThisManyActiveCustomers: amountActive thisManySuspendedCustomers: amountSuspended andThisManyTotalCustomers: amountTotal inThisBook: customerBook
	
	
	self assert: amountActive equals: customerBook numberOfActiveCustomers.
	self assert: amountSuspended equals: customerBook numberOfSuspendedCustomers.
	self assert:amountTotal equals: customerBook numberOfCustomers.
	
	"podriamos no parametrizar la unidadX y el customer, ya que estos se repiten en el código del test 5 y 6, 
	pero decidimos dejar los parametros por si se quieren hacer otros tests"
	
! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'f 4/12/2023 15:24:06'!
checkThatThisClosure: aClosure usingThisCustomerBook: customerBook takesLessThanThisAmountOfMilliseconds: amountOfMilliseconds
	  
	| millisecondsBeforeRunning millisecondsAfterRunning |
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClosure value: customerBook.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < (amountOfMilliseconds * millisecond)
! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'MC 4/12/2023 00:26:22'!
closureToCheckThatTheGeneratedErrorIsTheRightOneAndThisBookIsEmpty: customerBook

	^ [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'f 4/12/2023 15:10:05'!
closureToCheckThatThisBook: customerBook hasThisAmountOfCustomers: amountOfCostumers andIncludesThisCustomer:customerName 

	^ [ :anError | 
			self assert: customerBook numberOfCustomers = amountOfCostumers .
			self assert: (customerBook includesCustomerNamed: customerName ) ]! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'f 4/12/2023 14:46:27'!
ifThisClosure: aClosure generatesThisError: error thenCheckThat: conditionToVerify andCheckThat: otherConditionToVerify
	aClosure
		on: error
		do: [ :anError | 
			self assert: conditionToVerify.
			self assert: otherConditionToVerify ]! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'MC 4/12/2023 00:23:52'!
ifThisClosure: aClosure generatesThisError: error thenVerifyStatusUsing: verificationClosure
	aClosure
		on: error
		do: verificationClosure ! !


!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:24:22'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |
	
	customerBook := CustomerBook new.
	
	^self checkThatThisClosure: [:book| book addCustomerNamed: 'John Lennon']   usingThisCustomerBook: customerBook takesLessThanThisAmountOfMilliseconds: 50.
	

! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:24:30'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	
	^self checkThatThisClosure:[:book| book removeCustomerNamed: paulMcCartney. ]  usingThisCustomerBook: customerBook takesLessThanThisAmountOfMilliseconds: 100.

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:24:36'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.
	
	self ifThisClosure: [ customerBook addCustomerNamed: ''.
					self fail ]
	generatesThisError:  Error
	thenVerifyStatusUsing: (self closureToCheckThatTheGeneratedErrorIsTheRightOneAndThisBookIsEmpty: customerBook)
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:24:43'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self ifThisClosure: [ customerBook removeCustomerNamed: 'Paul McCartney'.
					 self fail  ]
	generatesThisError:  NotFound
	thenVerifyStatusUsing: (self closureToCheckThatThisBook: customerBook hasThisAmountOfCustomers: 1 andIncludesThisCustomer: johnLennon )
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:24:50'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self checkThatThereAreThisManyActiveCustomers: 0
	thisManySuspendedCustomers:  1
	andThisManyTotalCustomers:  1
	inThisBook: customerBook .

	self assert: (customerBook includesCustomerNamed: paulMcCartney).

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:24:55'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self checkThatThereAreThisManyActiveCustomers: 0
	thisManySuspendedCustomers:  0
	andThisManyTotalCustomers:  0
	inThisBook: customerBook .

	self deny: (customerBook includesCustomerNamed: paulMcCartney).

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:25:02'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self ifThisClosure: [ customerBook suspendCustomerNamed: 'George Harrison'.
					 self fail  ]
	generatesThisError:  CantSuspend
	thenVerifyStatusUsing: (self closureToCheckThatThisBook: customerBook hasThisAmountOfCustomers: 1 andIncludesThisCustomer: johnLennon )
	

! !

!CustomerBookTest methodsFor: 'testing' stamp: 'f 4/12/2023 15:25:09'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self ifThisClosure: [ customerBook suspendCustomerNamed: johnLennon.
					 self fail ]
	generatesThisError:  CantSuspend
	thenVerifyStatusUsing: (self closureToCheckThatThisBook: customerBook hasThisAmountOfCustomers: 1 andIncludesThisCustomer: johnLennon )
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'f 4/11/2023 20:51:02'!
RepeatedName: aName

	^ ((active includes: aName) or: [suspended includes: aName]) ifTrue: [ self signalCustomerAlreadyExists ]! !

!CustomerBook methodsFor: 'customer management' stamp: 'f 4/11/2023 20:50:17'!
addCustomerNamed: aName

	self checkForInvalidName: aName.
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'MC 4/12/2023 00:39:28'!
checkForEmptyName: aName

	^ aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ]! !

!CustomerBook methodsFor: 'customer management' stamp: 'MC 4/12/2023 00:39:28'!
checkForInvalidName: aName

	self checkForEmptyName: aName.
	self checkForRepeatedName: aName! !

!CustomerBook methodsFor: 'customer management' stamp: 'MC 4/12/2023 00:39:14'!
checkForRepeatedName: aName

	^ ((active includes: aName) or: [suspended includes: aName]) ifTrue: [ self signalCustomerAlreadyExists ]! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'MC 4/12/2023 00:34:16'!
remove: aName fromCollection: aCollection andOnSuccessDo: aClosure

	1 to: aCollection size do: 
		[ :index |
			aName = (aCollection at: index)
				ifTrue: [
					aCollection removeAt: index.
					aClosure value: aName 
				] 
		].! !

!CustomerBook methodsFor: 'customer management' stamp: 'f 4/12/2023 15:23:34'!
removeCustomerNamed: aName 
	
	self remove: aName fromCollection: active andOnSuccessDo: [:name | ^name].
	self remove: aName fromCollection: suspended andOnSuccessDo: [:name| ^name].

	^ NotFound signal.
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/9/2023 22:25:52'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/9/2023 22:25:56'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
