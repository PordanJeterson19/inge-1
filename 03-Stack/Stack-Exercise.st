!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'stackInstance sentenceFinderInstance'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'setup' stamp: 'MC 4/21/2023 19:13:57'!
setUp

	stackInstance := OOStack new.
	
	sentenceFinderInstance  := SentenceFinderByPrefix new.! !


!SentenceFinderByPrefixTest methodsFor: 'auxiliary - private' stamp: 'MC 4/23/2023 17:29:53'!
shouldUseOf: aSentencePrefix toFindSentencesRaiseErrorAssert: aClosureToVerify
	
	[sentenceFinderInstance findSentencesWithThisPrefix: aSentencePrefix inThisStack: stackInstance.
		self fail.]
		on: Error
		do: aClosureToVerify
	
	
	
	
	! !


!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'f 4/22/2023 18:28:53'!
test01SentenceFinderByPrefixShouldFindPrefixMatchingSentence

	|returnedSentences expected |
	
	stackInstance push:'Winter is coming'.

	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Win'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #('Winter is coming').
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'f 4/22/2023 18:14:33'!
test02SentenceFinderByPrefixShouldNotFindSentencesInEmptyStack

	|returnedSentences expected |
	
	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Wint'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #().
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'f 4/22/2023 18:17:15'!
test03SentenceFinderByPrefixShouldNotFindSentencesInStackWithoutPrefixMatchingSentences

	|returnedSentences expected |
	
	stackInstance push:'Vamo boca'.
	stackInstance push:'No me gusta boca'.
	stackInstance push:'Me siento indeferente acerca de boca'.
	
	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Wint'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #().
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'MC 4/23/2023 16:59:11'!
test04SentenceFinderByPrefixShouldFindPrefixMatchingSentencesInPoppingOrder

	|returnedSentences expected |
	
	stackInstance push: 'Winter is coming'.
	stackInstance push: 'Winrar is coming'.
	stackInstance push: 'Windhelm is coming'.
	
	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Win'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #('Windhelm is coming' 'Winrar is coming' 'Winter is coming').
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'MC 4/23/2023 17:02:04'!
test05SentenceFinderByPrefixShouldNotFindPrefixMatchingSentencesIfPrefixIsNotInTheStart

	|returnedSentences expected |
	
	stackInstance push: 'is Winter coming?'.
	stackInstance push: 'Winter is coming'.
	stackInstance push: 'Yes, Winter is coming'.
	
	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Win'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #('Winter is coming').
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'MC 4/23/2023 16:57:02'!
test06SentenceFinderByPrefixIsCaseSensitive

	|returnedSentences expected |
	
	stackInstance push: 'winter is coming'.
	
	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Win'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #().
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'f 4/22/2023 18:39:35'!
test07SentenceFinderByPrefixFindsRepeatedPrefixMatchingSentences

	|returnedSentences expected |
	
	stackInstance push:'Winter is coming'.
	stackInstance push:'Winter is coming'.
	stackInstance push:'Winrar is free'.
	stackInstance push:'Winter is coming'.
	
	returnedSentences := sentenceFinderInstance findSentencesWithThisPrefix: 'Win'  inThisStack: stackInstance.
	
	expected := OrderedCollection newFrom: #('Winter is coming' 'Winrar is free' 'Winter is coming' 'Winter is coming').
	
	self assert: returnedSentences equals: expected
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'f 4/22/2023 19:26:13'!
test08SentenceFinderByPrefixDoesNotModifyStack

	|expectedContent originalStackContent |
	
	stackInstance push:'Winter is coming'.
	stackInstance push:'Winrar is free'.
	stackInstance push:'hola bro'.
	
	sentenceFinderInstance findSentencesWithThisPrefix: 'Win' inThisStack: stackInstance.
	
	originalStackContent := OrderedCollection new.
	
	originalStackContent addFirst: stackInstance pop.
	originalStackContent addFirst: stackInstance pop.
	originalStackContent addFirst: stackInstance pop.
	
	expectedContent := OrderedCollection newFrom: #('Winter is coming' 'Winrar is free' 'hola bro'  ).
	
	self assert: (stackInstance size) equals: 0.
	
	self assert: originalStackContent equals: expectedContent 
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'MC 4/23/2023 17:29:53'!
test09SentenceFinderByPrefixDoesNotAcceptEmptyPrefix

	stackInstance push:'Winter is coming'.
	stackInstance push:'Winrar is free'.
	
	self shouldUseOf: '' 
	toFindSentencesRaiseErrorAssert: [ :anError | self assert: 
			anError messageText = SentenceFinderByPrefix prefixShouldNotBeEmptyErrorDescription]

	
	
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'MC 4/23/2023 17:42:31'!
test10SentenceFinderByPrefixDoesNotAcceptPrefixWithBlankSpaces
	
	stackInstance push:'Winter is coming'.
	stackInstance push:'Winrar is free'.
	
	self shouldUseOf: 'Winter is' 
	toFindSentencesRaiseErrorAssert:  [ :anError | self assert: 
			anError messageText = SentenceFinderByPrefix prefixShouldNotHaveBlankSpacesErrorDescription]
	

	
	
	
	
	! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'elements stackFlag'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'auxiliary - private' stamp: 'MC 4/21/2023 19:53:26'!
popAndSetFlag: newCurrentStackFlag

	stackFlag := newCurrentStackFlag.
	^elements removeFirst ! !

!OOStack methodsFor: 'auxiliary - private' stamp: 'MC 4/21/2023 19:50:57'!
pushAndSetFlag: elementToAdd

	elements addFirst: elementToAdd.
	stackFlag  := FlagStackIsNotEmpty withPreviousFlag: stackFlag.! !

!OOStack methodsFor: 'auxiliary - private' stamp: 'MC 4/21/2023 19:41:07'!
safeTop

	^elements first ! !


!OOStack methodsFor: 'initialization' stamp: 'MC 4/21/2023 19:46:41'!
initialize

	elements  := OrderedCollection new.
	
	stackFlag  := FlagStackIsEmpty new.! !


!OOStack methodsFor: 'accessing' stamp: 'MC 4/21/2023 19:41:12'!
size
	
	^elements size! !

!OOStack methodsFor: 'accessing' stamp: 'f 4/22/2023 19:06:59'!
top

	^stackFlag assertThatTopIsSafe: self
	! !


!OOStack methodsFor: 'stack manipulation' stamp: 'f 4/22/2023 19:05:58'!
pop

	^stackFlag assertThatPopIsSafe: self.
! !

!OOStack methodsFor: 'stack manipulation' stamp: 'f 4/22/2023 18:58:14'!
push: elementToAdd

	self pushAndSetFlag: elementToAdd! !


!OOStack methodsFor: 'testing' stamp: 'MC 4/21/2023 19:40:42'!
isEmpty

	^elements isEmpty ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'find' stamp: 'MC 4/23/2023 16:24:40'!
findSentencesWithThisPrefix: prefixSentence inThisStack: stackThatContainsSentences

	|poppedSentences  selectedSentences|
	
	self assertThatPrefixIsValid: prefixSentence.
	
	poppedSentences  := OOStack new.

	selectedSentences := self   popAllSentencesIn:  stackThatContainsSentences 
							selectThoseWithThisPrefix: prefixSentence 
							andSavePoppedSentencesIn: poppedSentences .
	
	self reconstruct: stackThatContainsSentences with: poppedSentences.
					
	^selectedSentences ! !


!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'MC 4/23/2023 17:44:12'!
assertThatPrefixHasNoBlankSpaces: prefixSentence
	
	(self prefixHasNoBlankSpaces: prefixSentence) ifFalse:
	[self error: self class prefixShouldNotHaveBlankSpacesErrorDescription].
	! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'MC 4/23/2023 17:40:21'!
assertThatPrefixIsNotEmpty: prefixSentence

	(prefixSentence isEmpty) ifTrue: [self error: self class prefixShouldNotBeEmptyErrorDescription].
! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'MC 4/23/2023 17:50:51'!
assertThatPrefixIsValid: prefixSentence

	self assertThatPrefixHasNoBlankSpaces: prefixSentence.
	self assertThatPrefixIsNotEmpty: prefixSentence.! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'MC 4/23/2023 17:45:54'!
popAllSentencesIn: stackThatContainsSentences selectThoseWithThisPrefix: prefixSentence andSavePoppedSentencesIn: poppedSentences 

	|equalPrefixSentences|
	
	equalPrefixSentences := OrderedCollection  new.

	1 to: stackThatContainsSentences size do:[:index| self popSentenceFrom: stackThatContainsSentences 
												andSaveItIn: poppedSentences
												andShouldItHave: prefixSentence 
												thenAddItTo: equalPrefixSentences].
		
	^equalPrefixSentences 
		
	

! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'f 4/22/2023 18:49:24'!
popSentenceFrom: stackThatContainsSentences andSaveItIn: poppedSentences andShouldItHave: prefixSentence thenAddItTo: equalPrefixSentences

	|nextSentence|
	
	nextSentence := stackThatContainsSentences pop.
	self should: nextSentence have: prefixSentence thenAddItTo: equalPrefixSentences .
	poppedSentences push: nextSentence
	
	
	
		
	

! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'MC 4/23/2023 17:45:05'!
prefixHasNoBlankSpaces: prefixSentence
	
	^(prefixSentence findString: ' ') = 0
	! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'f 4/22/2023 18:49:56'!
reconstruct: stackThatContainsSentences with: poppedSentences
	
	1 to: (poppedSentences size) do: [:index| stackThatContainsSentences push: (poppedSentences pop)].
					
! !

!SentenceFinderByPrefix methodsFor: 'auxiliary - private' stamp: 'f 4/22/2023 17:58:42'!
should: sentence have: prefixSentence thenAddItTo: equalPrefixSentences

	((sentence findString: prefixSentence) = 1) ifTrue: [equalPrefixSentences addLast: sentence ].

	
	
	
		
	

! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'as yet unclassified' stamp: 'MC 4/23/2023 16:11:15'!
prefixShouldNotBeEmptyErrorDescription
	
	^ 'prefix should not be empty!!!!!!'! !

!SentenceFinderByPrefix class methodsFor: 'as yet unclassified' stamp: 'MC 4/23/2023 16:14:26'!
prefixShouldNotHaveBlankSpacesErrorDescription
	
	^ 'prefix should not have blank spaces!!!!!!'! !


!classDefinition: #StackFlags category: 'Stack-Exercise'!
Object subclass: #StackFlags
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackFlags methodsFor: 'as yet unclassified' stamp: 'f 4/22/2023 19:05:42'!
assertThatPopIsSafe: stackToPopTopElementFrom

	self subclassResponsibility ! !

!StackFlags methodsFor: 'as yet unclassified' stamp: 'f 4/22/2023 19:06:29'!
assertThatTopIsSafe: stackToCheckTopElementFrom

	self subclassResponsibility ! !


!classDefinition: #FlagStackIsEmpty category: 'Stack-Exercise'!
StackFlags subclass: #FlagStackIsEmpty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!FlagStackIsEmpty methodsFor: 'as yet unclassified' stamp: 'f 4/22/2023 19:05:42'!
assertThatPopIsSafe: stackToPopTopElementFrom

	stackToPopTopElementFrom error: OOStack stackEmptyErrorDescription ! !

!FlagStackIsEmpty methodsFor: 'as yet unclassified' stamp: 'f 4/22/2023 19:06:29'!
assertThatTopIsSafe: stackToCheckTopElementFrom

	stackToCheckTopElementFrom error: OOStack stackEmptyErrorDescription ! !


!classDefinition: #FlagStackIsNotEmpty category: 'Stack-Exercise'!
StackFlags subclass: #FlagStackIsNotEmpty
	instanceVariableNames: 'previousFlag'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!FlagStackIsNotEmpty methodsFor: 'as yet unclassified' stamp: 'f 4/22/2023 19:05:42'!
assertThatPopIsSafe: stackToPopTopElementFrom
	
	^stackToPopTopElementFrom popAndSetFlag: previousFlag.
! !

!FlagStackIsNotEmpty methodsFor: 'as yet unclassified' stamp: 'f 4/22/2023 19:06:29'!
assertThatTopIsSafe: stackToCheckTopElementFrom

	^stackToCheckTopElementFrom safeTop! !

!FlagStackIsNotEmpty methodsFor: 'as yet unclassified' stamp: 'MC 4/21/2023 19:56:40'!
initializeWith: flagThatIsNoLongerCurrent
	
	previousFlag := flagThatIsNoLongerCurrent! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FlagStackIsNotEmpty class' category: 'Stack-Exercise'!
FlagStackIsNotEmpty class
	instanceVariableNames: ''!

!FlagStackIsNotEmpty class methodsFor: 'instance creation' stamp: 'MC 4/21/2023 19:57:26'!
withPreviousFlag: flagThatIsNoLongerCurrent

	^self new initializeWith: flagThatIsNoLongerCurrent! !
