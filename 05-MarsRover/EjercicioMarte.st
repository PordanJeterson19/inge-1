!classDefinition: #MarsRoverTest category: 'EjercicioMarte'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!MarsRoverTest methodsFor: 'assertions' stamp: 'MC 5/14/2023 20:18:10'!
assertMarsRover: aMarsRover isAt: aPosition facing: aCardinalDirection

	
	self assert: aPosition equals: aMarsRover position.
	self assert: aCardinalDirection equals: aMarsRover direction class .! !

!MarsRoverTest methodsFor: 'assertions' stamp: 'MC 5/14/2023 20:24:55'!
assertMarsRoverRaisesErrorIfItReadsInvalidCommand: aMarsRover

	
	self should: [aMarsRover process: 't'. 
				self fail] 
        raise: Error
        withExceptionDo: [:anException | 
            		self assert: 'Invalid command, processing stopped' equals: anException messageText. 
		self assertMarsRover: aMarsRover isAt: (0@0) facing: North
        ].
	
! !

!MarsRoverTest methodsFor: 'assertions' stamp: 'MC 5/14/2023 20:19:01'!
assertMarsRoverThatProcesses: aStringWithCommands endsAtPosition: expectedPosition facing: facedCardinalDirection

	
	| aMarsRover |
	aMarsRover := self NorthFacingMarsRover.
	aMarsRover process: aStringWithCommands.

	self assertMarsRover: aMarsRover isAt: expectedPosition facing: facedCardinalDirection! !


!MarsRoverTest methodsFor: 'mars rover creation' stamp: 'MC 5/14/2023 20:03:42'!
NorthFacingMarsRover

	^ MarsRover at: (0@0) facing: North new! !


!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:20:30'!
test01SeInicializaEnLaPosicionDadaYMirandoALaDireccionDada

	| aMarsRover |
	
	aMarsRover := self NorthFacingMarsRover.

	self assertMarsRover: aMarsRover isAt: (0@0) facing: North! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:40:39'!
test02LecturaDeComandoVacioNoHaceNada
	
	
	self assertMarsRoverThatProcesses: '' endsAtPosition: (0@0) facing: North! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:40:48'!
test03LecturaDeComandoAvanzarAvanzaUnEspacio

	
	self assertMarsRoverThatProcesses: 'f' endsAtPosition: (0@1) facing: North! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:40:57'!
test04LecturaDeComandoRetrocederRetrocedeUnEspacio

	
	self assertMarsRoverThatProcesses: 'b' endsAtPosition: (0@-1) facing: North! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:41:28'!
test05LecturaDeComandoGirarDerechaGiraNoventaGradosALaDerecha

	
	self assertMarsRoverThatProcesses: 'r' endsAtPosition: (0@0) facing: East! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:41:38'!
test06LecturaDeComandoGirarIzquierdaGiraNoventaGradosALaIzquierda

	
	self assertMarsRoverThatProcesses: 'l' endsAtPosition: (0@0) facing: West! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:41:52'!
test07LecturaDeComandoErroneoLevantaErrorDeComandoInvalido
	
	| aMarsRover |
	
	aMarsRover := self NorthFacingMarsRover.
	
	self assertMarsRoverRaisesErrorIfItReadsInvalidCommand: aMarsRover
	
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:42:02'!
test08LecturaDeMultiplesComandosAvanzarAvanzaMultiplesVeces
	
	
	self assertMarsRoverThatProcesses: 'fff' endsAtPosition: (0@3) facing: North! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:42:10'!
test09LecturaDeMultiplesComandosRetrocederRetrocedeMultiplesVeces
	
	
	self assertMarsRoverThatProcesses: 'bbb' endsAtPosition: (0@-3) facing: North! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:42:20'!
test10LecturaDeMultiplesComandosGirarDerechaGiraMultiplesVeces

	
	self assertMarsRoverThatProcesses: 'rrr' endsAtPosition: (0@0) facing: West! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:42:30'!
test11LecturaDeMultiplesComandosGirarIzquierdaGiraMultiplesVeces

	
	self assertMarsRoverThatProcesses: 'lll' endsAtPosition: (0@0) facing: East! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:26:08'!
test12LecturaDeComandosGirarYAvanzarAvanzaEnDireccionCorrecta

	
	self assertMarsRoverThatProcesses: 'rf' endsAtPosition: (1@0) facing: East! !

!MarsRoverTest methodsFor: 'tests' stamp: 'MC 5/14/2023 20:26:11'!
test13LecturaDeComandosGirarYretrocederRetrocedeEnDireccionCorrecta

	
	self assertMarsRoverThatProcesses: 'rb' endsAtPosition: (-1@0) facing: East! !


!classDefinition: #CardinalPoints category: 'EjercicioMarte'!
Object subclass: #CardinalPoints
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!CardinalPoints methodsFor: 'turning' stamp: 'asd 5/13/2023 21:22:18'!
turnLeft

	self subclassResponsibility ! !

!CardinalPoints methodsFor: 'turning' stamp: 'asd 5/13/2023 21:21:54'!
turnRight

	self subclassResponsibility ! !


!CardinalPoints methodsFor: 'movement' stamp: 'asd 5/13/2023 21:27:56'!
moveBackwards: aMarsRover

	self subclassResponsibility ! !

!CardinalPoints methodsFor: 'movement' stamp: 'asd 5/13/2023 21:27:32'!
moveForward: aMarsRover

	self subclassResponsibility ! !


!classDefinition: #East category: 'EjercicioMarte'!
CardinalPoints subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!East methodsFor: 'movement' stamp: 'MC 5/14/2023 19:51:49'!
moveBackwards: aMarsRover

	aMarsRover moveWest .! !

!East methodsFor: 'movement' stamp: 'MC 5/14/2023 19:51:56'!
moveForward: aMarsRover

	aMarsRover moveEast.! !


!East methodsFor: 'turning' stamp: 'asd 5/13/2023 21:22:18'!
turnLeft

	^North new.! !

!East methodsFor: 'turning' stamp: 'asd 5/13/2023 21:21:54'!
turnRight

	^South new.! !


!classDefinition: #North category: 'EjercicioMarte'!
CardinalPoints subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!North methodsFor: 'movement' stamp: 'MC 5/14/2023 19:51:42'!
moveBackwards: aMarsRover

	aMarsRover moveSouth .! !

!North methodsFor: 'movement' stamp: 'asd 5/13/2023 21:27:32'!
moveForward: marsRover

	marsRover moveNorth.! !


!North methodsFor: 'turning' stamp: 'asd 5/13/2023 21:22:18'!
turnLeft

	^West new.! !

!North methodsFor: 'turning' stamp: 'asd 5/13/2023 21:21:54'!
turnRight

	^East new.! !


!classDefinition: #South category: 'EjercicioMarte'!
CardinalPoints subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!South methodsFor: 'movement' stamp: 'MC 5/14/2023 19:51:20'!
moveBackwards: aMarsRover

	aMarsRover moveNorth .! !

!South methodsFor: 'movement' stamp: 'MC 5/14/2023 20:36:04'!
moveForward: aMarsRover

	aMarsRover moveSouth.! !


!South methodsFor: 'turning' stamp: 'asd 5/13/2023 21:22:18'!
turnLeft

	^East new.! !

!South methodsFor: 'turning' stamp: 'asd 5/13/2023 21:21:54'!
turnRight

	^West new.! !


!classDefinition: #West category: 'EjercicioMarte'!
CardinalPoints subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!West methodsFor: 'movement' stamp: 'MC 5/14/2023 19:51:14'!
moveBackwards: aMarsRover

	aMarsRover moveWest .! !

!West methodsFor: 'movement' stamp: 'MC 5/14/2023 19:51:06'!
moveForward: aMarsRover

	aMarsRover moveSouth.! !


!West methodsFor: 'turning' stamp: 'asd 5/13/2023 21:22:19'!
turnLeft

	^South new.! !

!West methodsFor: 'turning' stamp: 'asd 5/13/2023 21:21:54'!
turnRight

	^North new.! !


!classDefinition: #MarsRover category: 'EjercicioMarte'!
Object subclass: #MarsRover
	instanceVariableNames: 'position direction instructions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjercicioMarte'!

!MarsRover methodsFor: 'turning-auxiliary' stamp: 'asd 5/13/2023 21:22:19'!
turnLeft

	direction  := direction turnLeft 
! !

!MarsRover methodsFor: 'turning-auxiliary' stamp: 'asd 5/13/2023 21:21:54'!
turnRight

	direction := direction turnRight 
! !


!MarsRover methodsFor: 'processing - auxiliary' stamp: 'MC 5/14/2023 19:52:48'!
processCommand: aCommandCharacter
	
	(instructions at: aCommandCharacter ifAbsent: [self error: MarsRover errorInvalidCommand]) value
	
	
	
	
! !


!MarsRover methodsFor: 'processing' stamp: 'MC 5/14/2023 19:50:20'!
process: aStringWithCommands
	
	
	aStringWithCommands = '' ifTrue: [^self].
	
	aStringWithCommands do: [:aCommandCharacter| self processCommand: aCommandCharacter]
	
	
	
	
! !


!MarsRover methodsFor: 'initialization' stamp: 'MC 5/14/2023 19:43:47'!
initializeWith: a2CoordinatePoint with: aCardinalPoint

	instructions := self marsRoverCommandDictionary .
	position := a2CoordinatePoint .
	direction := aCardinalPoint .! !


!MarsRover methodsFor: 'accessing' stamp: 'A 5/11/2023 21:06:22'!
direction

	^direction ! !

!MarsRover methodsFor: 'accessing' stamp: 'asd 5/12/2023 17:40:31'!
position

	^position! !


!MarsRover methodsFor: 'moving-auxiliary' stamp: 'MC 5/14/2023 20:31:11'!
moveByAddingRelativeMovement: aRelativeMovement

	position := position + aRelativeMovement! !

!MarsRover methodsFor: 'moving-auxiliary' stamp: 'MC 5/14/2023 20:39:02'!
moveEast

	self moveByAddingRelativeMovement: (1@0)! !

!MarsRover methodsFor: 'moving-auxiliary' stamp: 'MC 5/14/2023 20:39:06'!
moveNorth

	self moveByAddingRelativeMovement: (0@1)! !

!MarsRover methodsFor: 'moving-auxiliary' stamp: 'MC 5/14/2023 20:39:26'!
moveSouth

	self moveByAddingRelativeMovement: (0@-1)! !

!MarsRover methodsFor: 'moving-auxiliary' stamp: 'MC 5/14/2023 20:39:30'!
moveWest

	self moveByAddingRelativeMovement: (-1@0)! !


!MarsRover methodsFor: 'dictionary creation - auxiliary' stamp: 'MC 5/14/2023 19:43:35'!
marsRoverCommandDictionary


	| commandDictionary |
	
	commandDictionary := Dictionary new.	
	commandDictionary at: $f put: [direction moveForward: self].
	commandDictionary at: $b put: [direction moveBackwards: self].
	commandDictionary at: $l put: [self turnLeft].
	commandDictionary at: $r put: [self turnRight].
	
	^commandDictionary
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'EjercicioMarte'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'asd 5/13/2023 21:18:51'!
at: a2PointCoordenate facing: aCardinalPoint

	^MarsRover new initializeWith: a2PointCoordenate with: aCardinalPoint ! !


!MarsRover class methodsFor: 'error description' stamp: 'MC 5/14/2023 19:53:06'!
errorInvalidCommand

	^'Invalid command, processing stopped'! !
