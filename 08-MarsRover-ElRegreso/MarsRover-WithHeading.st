!classDefinition: #LogWindowTest category: 'MarsRover-WithHeading'!
TestCase subclass: #LogWindowTest
	instanceVariableNames: 'log window marsRoverWithLog marsRoverWithWindow'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!LogWindowTest methodsFor: 'setup' stamp: 'mc 6/4/2023 06:01:34'!
setUp

	| marsRover1 marsRover2 |
	
	marsRover1 := self marsRoverHeadingNorthAtZeroPoint.
	
	log := Log for: marsRover1.
	
	marsRoverWithLog := marsRover1 addObserver: log.
		
	
	marsRover2 := self marsRoverHeadingNorthAtZeroPoint.
	
	window := Window for: marsRover2.
	
	marsRoverWithWindow := marsRover2 addObserver: window.
	
	
	! !


!LogWindowTest methodsFor: 'mars rover building' stamp: 'mc 6/4/2023 05:47:33'!
marsRoverHeadingNorthAtZeroPoint

	^ MarsRover at: 0@0 heading: MarsRoverHeadingNorth.

	
	
	! !


!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test01PositionsOfMarsRoverLogShowsNewPositionAfterOneMovement

	
	marsRoverWithLog process: 'f'.
	
	self assert: 
'0@1' 
	equals: (log printPositions).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test02PositionOfMarsRoverWindowShowsLastPositionAfterProcessingAMovement


	marsRoverWithWindow process: 'f'.
	
	self assert: 
'0@1' 
	equals: (window printPosition).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test03PositionsOfMarsRoverLogShowsAllPositionsSinceLogWasAddedAfterManyMovements

	
	marsRoverWithLog process: 'fff'.
	
	self assert: 
'0@1
0@2
0@3' 
	equals: (log printPositions).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test04PositionOfMarsRoverWindowShowsLastPositionAfterProcessingManyMovements

	
	marsRoverWithWindow process: 'fff'.
	
	self assert: 
'0@3' 
	equals: (window printPosition).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test05HeadingsOfMarsRoverLogShowsNewHeadingAfterProcessingRotation


	marsRoverWithLog process: 'r'.
	
	self assert: 
'Apuntando al Este' 
	equals: (log printHeadings).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test06HeadingsOfMarsRoverLogShowsAllHeadingsSinceLogWasAddedAfterManyRotations


	marsRoverWithLog process: 'rrrr'.
	
	self assert: 
'Apuntando al Este
Apuntando al Sur
Apuntando al Oeste
Apuntando al Norte' 
	equals: (log printHeadings).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test07HeadingOfMarsRoverWindowShowsNewHeadingAfterProcessingRotation

	
	marsRoverWithWindow process: 'r'.
	
	self assert: 
'Apuntando al Este' 
	equals: (window printHeading).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test08HeadingOfMarsRoverWindowShowsLastHeadingAfterProcessingManyRotations

	
	marsRoverWithWindow process: 'rrrr'.
	
	self assert: 
'Apuntando al Norte' 
	equals: (window printHeading).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test09PositionsOfMarsRoverLogShowsOnlyPositionsSinceLogWasAddedAfterManyOperations

	
	marsRoverWithLog process: 'frfrfr'.
	
	self assert: 
'0@1
1@1
1@0' 
	equals: (log printPositions).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test10HeadingsOfMarsRoverLogShowsOnlyHeadingsSinceLogWasAddedAfterManyOperations

	
	marsRoverWithLog process: 'frfrfr'.
	
	self assert: 
'Apuntando al Este
Apuntando al Sur
Apuntando al Oeste' 
	equals: (log printHeadings ).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test11FullLogOfMarsRoverLogShowsNewPositionAfterProcessingMovement

	
	marsRoverWithLog process: 'f'.
	
	self assert: 
'0@1' 
	equals: (log printFullLog).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test12FullLogOfMarsRoverLogShowsNewHeadingAfterProcessingRotation

	
	marsRoverWithLog process: 'r'.
	
	self assert: 
'Apuntando al Este' 
	equals: (log printFullLog).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test13FullLogOfMarsRoverLogShowsAllPositionsSinceLogWasAddedAfterManyMovements

	
	marsRoverWithLog process: 'fff'.
	
	self assert: 
'0@1
0@2
0@3' 
	equals: (log printFullLog).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test14FullLogOfMarsRoverLogShowsAllHeadingsSinceLogWasAddedAfterManyRotations

	
	marsRoverWithLog process: 'rrrr'.
	
	self assert: 
'Apuntando al Este
Apuntando al Sur
Apuntando al Oeste
Apuntando al Norte' 
	equals: (log printFullLog).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:25'!
test15FullLogOfMarsRoverLogShowsAllHeadingsAndPositionsSinceLogWasAddedAfterProcessingCommands

	
	marsRoverWithLog process: 'frfrfr'.
	
	self assert: 
'0@1
Apuntando al Este
1@1
Apuntando al Sur
1@0
Apuntando al Oeste' 
	equals: (log printFullLog).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test16HeadingOfMarsRoverWindowShowsLastHeadingAfterProcessingManyCommands

	
	marsRoverWithWindow process: 'rf'.
	
	self assert: 
'Apuntando al Este' 
	equals: (window printHeading).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test17PositionOfMarsRoverWindowShowsLastHeadingAfterProcessingManyCommands

	
	marsRoverWithWindow process: 'fr'.
	
	self assert: 
'0@1' 
	equals: (window printPosition ).
	! !

!LogWindowTest methodsFor: 'tests' stamp: 'mc 6/4/2023 06:01:34'!
test18FullStateOfMarsRoverWindowShowsLastHeadingAfterProcessingManyCommands

	
	marsRoverWithWindow process: 'fr'.
	
	self assert: 
'0@1
Apuntando al Este' 
	equals: (window printFullState ).
	! !


!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head observers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:07:41'!
headingEastStateFor: anObserver

	^anObserver headingEastEntry! !

!MarsRover methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:08:09'!
headingNorthStateFor: anObserver

	^anObserver headingNorthEntry! !

!MarsRover methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:07:52'!
headingSouthStateFor: anObserver

	^anObserver headingSouthEntry! !

!MarsRover methodsFor: 'observer - private' stamp: 'asd 6/4/2023 19:37:44'!
headingStateFor: anObserver
	
	^head stateFor: anObserver 
	
	! !

!MarsRover methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:07:30'!
headingWestStateFor: anObserver

	^anObserver headingWestEntry! !

!MarsRover methodsFor: 'observer - private' stamp: 'asd 6/4/2023 18:36:22'!
notifyObservers: anInstanceVariable

	observers do: [:observer| observer update: anInstanceVariable]! !


!MarsRover methodsFor: 'observer' stamp: 'asd 6/3/2023 16:38:51'!
addObserver: anObserver

	observers addLast: anObserver 
! !


!MarsRover methodsFor: 'initialization' stamp: 'asd 6/3/2023 16:41:39'!
initializeAt: aPosition heading: aHeadingType

	observers := OrderedCollection new.
	position := aPosition.
	head := aHeadingType for: self .
	
! !


!MarsRover methodsFor: 'heading' stamp: 'asd 6/4/2023 19:37:17'!
headEast
	
	head := MarsRoverHeadingEast for: self.
	
	self notifyObservers: 'head'

! !

!MarsRover methodsFor: 'heading' stamp: 'asd 6/4/2023 19:37:20'!
headNorth
	
	head := MarsRoverHeadingNorth for: self .
	
	self notifyObservers: 'head'

	
! !

!MarsRover methodsFor: 'heading' stamp: 'asd 6/4/2023 19:37:23'!
headSouth
	
	head := MarsRoverHeadingSouth for: self.
	
	self notifyObservers: 'head'
	

! !

!MarsRover methodsFor: 'heading' stamp: 'asd 6/4/2023 19:37:27'!
headWest
	
	head := MarsRoverHeadingWest for: self .
	
	self notifyObservers: 'head'


! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'asd 6/4/2023 19:38:05'!
moveEast
	
	position := position + (1@0).
	
	self notifyObservers: 'position'

	

! !

!MarsRover methodsFor: 'moving' stamp: 'asd 6/3/2023 18:47:56'!
moveForward
	
	head moveForward! !

!MarsRover methodsFor: 'moving' stamp: 'asd 6/4/2023 19:38:09'!
moveNorth
	
	position := position + (0@1).
	
	self notifyObservers: 'position'
	


! !

!MarsRover methodsFor: 'moving' stamp: 'asd 6/4/2023 19:38:17'!
moveSouth
	
	position := position + (0@-1).
	
	self notifyObservers: 'position'

	! !

!MarsRover methodsFor: 'moving' stamp: 'asd 6/4/2023 19:38:21'!
moveWest
	
	position := position + (-1@0).
	
	self notifyObservers: 'position'

! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'asd 6/2/2023 15:42:58'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'accessing' stamp: 'asd 6/3/2023 17:49:35'!
positionState

	^position ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !


!MarsRoverHeading methodsFor: 'observer - private' stamp: 'asd 6/4/2023 19:47:23'!
stateFor: anObserver

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'asd 6/3/2023 18:04:07'!
stateFor: anObserver

	^marsRover headingEastStateFor: anObserver! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !



!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:04:26'!
stateFor: anObserver

	^marsRover headingNorthStateFor: anObserver! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:04:39'!
stateFor: anObserver

	^marsRover headingSouthStateFor: anObserver! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'observer - private' stamp: 'asd 6/3/2023 18:05:11'!
stateFor: anObserver

	^marsRover headingWestStateFor: anObserver! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!classDefinition: #Observer category: 'MarsRover-WithHeading'!
Object subclass: #Observer
	instanceVariableNames: 'updateDictionary marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Observer methodsFor: 'initialization' stamp: 'asd 6/4/2023 19:12:24'!
initializeFor: aMarsRover

	self subclassResponsibility 
	! !


!Observer methodsFor: 'update' stamp: 'asd 6/4/2023 19:08:56'!
update: aMarsRoverInstanceVariableString

	(updateDictionary at: aMarsRoverInstanceVariableString ifAbsent:[^self])value
	! !



!Observer methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 19:11:11'!
createUpdateDictionary

	self subclassResponsibility 
	! !


!Observer methodsFor: 'heading entry - private' stamp: 'asd 6/4/2023 19:11:24'!
headingEastEntry

	self subclassResponsibility 
	! !

!Observer methodsFor: 'heading entry - private' stamp: 'asd 6/4/2023 19:11:39'!
headingNorthEntry

	self subclassResponsibility 
	! !

!Observer methodsFor: 'heading entry - private' stamp: 'asd 6/4/2023 19:11:33'!
headingSouthEntry

	self subclassResponsibility 
	! !

!Observer methodsFor: 'heading entry - private' stamp: 'asd 6/4/2023 19:11:29'!
headingWestEntry

	self subclassResponsibility 
	! !


!classDefinition: #Log category: 'MarsRover-WithHeading'!
Observer subclass: #Log
	instanceVariableNames: 'log'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Log methodsFor: 'initialization' stamp: 'asd 6/4/2023 18:49:56'!
initializeFor: aMarsRover
	
	updateDictionary  := self createUpdateDictionary.
	marsRover := aMarsRover .
	log := OrderedCollection new.! !


!Log methodsFor: 'printing - private' stamp: 'asd 6/3/2023 16:16:57'!
add: aLoggedState to: aLogString

	|newString|
	newString := aLogString.
	aLogString isEmpty ifFalse: [newString := newString , String newLineString].
	newString := newString, aLoggedState asString.
	^newString 
	! !

!Log methodsFor: 'printing - private' stamp: 'asd 6/4/2023 19:30:04'!
printEntriesIfTrue: aBooleanBlock

	| positionsString |
	positionsString := String new.
	log do: [:loggedState |  (aBooleanBlock value: loggedState) ifTrue:[ positionsString := self add: loggedState to: positionsString] ].
	
	 ^positionsString
	
	
	! !


!Log methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 19:35:46'!
addHeadingEntry

	log addLast: (marsRover headingStateFor: self)
! !

!Log methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 19:35:31'!
addPositionEntry

	log addLast: (marsRover positionState)

	
! !

!Log methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 18:49:41'!
createUpdateDictionary

	| commandDictionary |

	commandDictionary := Dictionary new.
	commandDictionary at: 'position' put: [log addLast: (marsRover positionState)].
	commandDictionary at: 'head' put: [log addLast: (marsRover headingStateFor: self)].
	
	^commandDictionary ! !


!Log methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:10:08'!
headingEastEntry

	^'Apuntando al Este'! !

!Log methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:09:57'!
headingNorthEntry

	^'Apuntando al Norte'! !

!Log methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:10:15'!
headingSouthEntry

	^'Apuntando al Sur'! !

!Log methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:10:24'!
headingWestEntry

	^'Apuntando al Oeste'! !


!Log methodsFor: 'printing' stamp: 'asd 6/4/2023 19:32:19'!
printFullLog

		^self printEntriesIfTrue:  [:loggedState| true]! !

!Log methodsFor: 'printing' stamp: 'asd 6/4/2023 19:31:50'!
printHeadings

	^self printEntriesIfTrue:  [:loggedState| loggedState isKindOf: String]

	
	! !

!Log methodsFor: 'printing' stamp: 'asd 6/4/2023 19:31:17'!
printPositions

	^self printEntriesIfTrue:  [:loggedState| loggedState isKindOf: Point]
	
	
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Log class' category: 'MarsRover-WithHeading'!
Log class
	instanceVariableNames: ''!

!Log class methodsFor: 'as yet unclassified' stamp: 'asd 6/1/2023 20:49:15'!
for: aMarsRover

	^ self new initializeFor: aMarsRover .! !


!classDefinition: #Window category: 'MarsRover-WithHeading'!
Observer subclass: #Window
	instanceVariableNames: 'headingState positionState'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Window methodsFor: 'printing' stamp: 'asd 6/3/2023 16:32:14'!
printFullState

	^positionState , String newLineString , headingState ! !

!Window methodsFor: 'printing' stamp: 'asd 6/3/2023 16:21:47'!
printHeading

	^headingState ! !

!Window methodsFor: 'printing' stamp: 'asd 6/3/2023 16:21:52'!
printPosition

	^positionState ! !


!Window methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 19:18:38'!
createUpdateDictionary

	| commandDictionary |

	commandDictionary := Dictionary new.
	commandDictionary at: 'position' put: [self updatePosition ].
	commandDictionary at: 'head' put: [self updateHeading ].
	
	^commandDictionary ! !

!Window methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 19:18:07'!
updateHeading

	headingState := marsRover headingStateFor: self
	! !

!Window methodsFor: 'update dictionary - private' stamp: 'asd 6/4/2023 19:17:39'!
updatePosition

	positionState  := (marsRover positionState) asString
	! !


!Window methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:19:57'!
headingEastEntry

	^'Apuntando al Este'! !

!Window methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:20:11'!
headingNorthEntry

	^'Apuntando al Norte'! !

!Window methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:20:19'!
headingSouthEntry

	^'Apuntando al Sur'! !

!Window methodsFor: 'heading entry - private' stamp: 'asd 6/3/2023 18:20:28'!
headingWestEntry

	^'Apuntando al Oeste'! !


!Window methodsFor: 'initialization' stamp: 'asd 6/4/2023 18:49:17'!
initializeFor: aMarsRover

	updateDictionary  := self createUpdateDictionary .
	marsRover := aMarsRover .
	positionState := ''.
	headingState := ''! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Window class' category: 'MarsRover-WithHeading'!
Window class
	instanceVariableNames: ''!

!Window class methodsFor: 'as yet unclassified' stamp: 'asd 6/1/2023 20:48:59'!
for: aMarsRover

	^ self new initializeFor: aMarsRover .! !
