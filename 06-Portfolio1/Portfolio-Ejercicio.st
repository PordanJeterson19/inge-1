!classDefinition: #PortfolioTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'assertions' stamp: 'MC 5/20/2023 00:53:37'!
assertEmptyPortfolio: anEmptyPortfolio failsToAdd: componentThatCannotBeAdded

	self assertPortfolio: anEmptyPortfolio 
		failsToAdd: componentThatCannotBeAdded 
		hasThisManyComponents: 0
		thenDo: [].! !

!PortfolioTest methodsFor: 'assertions' stamp: 'MC 5/20/2023 00:51:18'!
assertPortfolio: aPortfolio failsToAdd: componentThatCannotBeAdded hasThisManyComponents: amountOfComponents thenDo: blockThatVerifiesContents

	self should: [aPortfolio add: componentThatCannotBeAdded.
				self fail] 
        raise: Error
        withExceptionDo: [:anException | 
            		self assert: 'Portfolio cannot have same element multiple times.' equals: anException messageText. 
		self assert: amountOfComponents equals: aPortfolio contents size.
		blockThatVerifiesContents value
        ].! !

!PortfolioTest methodsFor: 'assertions' stamp: 'MC 5/20/2023 00:55:05'!
assertPortfolio: aPortfolioWithOneComponent withOneComponent: componentInPortfolio failsToAdd: componentThatCannotBeAdded

	self assertPortfolio: aPortfolioWithOneComponent
		failsToAdd: componentThatCannotBeAdded
		hasThisManyComponents: 1
		thenDo: [self assert: aPortfolioWithOneComponent contents first = componentInPortfolio]! !


!PortfolioTest methodsFor: 'accounts' stamp: 'MC 5/20/2023 02:09:42'!
accountWithDepositOf100

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	^account
! !

!PortfolioTest methodsFor: 'accounts' stamp: 'MC 5/20/2023 02:09:48'!
accountWithDepositOf100AndWithdrawalOf25
	
	| account |
	
	account := self accountWithDepositOf100.
	Withdraw register: 25 on: account.
	^account
! !

!PortfolioTest methodsFor: 'accounts' stamp: 'MC 5/20/2023 02:03:10'!
accountWithDepositOf175

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 175 on: account.
	^account! !

!PortfolioTest methodsFor: 'accounts' stamp: 'MC 5/20/2023 02:03:04'!
accountWithDepositOf25

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 25 on: account.
	^account! !


!PortfolioTest methodsFor: 'portfolios' stamp: 'MC 5/20/2023 17:09:24'!
portfolioWithComponent: anAccountOrPortfolio

	| portfolio |
	
	portfolio := Portfolio new.
	
	portfolio add: anAccountOrPortfolio.
	
	^ portfolio! !

!PortfolioTest methodsFor: 'portfolios' stamp: 'MC 5/21/2023 18:09:56'!
portfolioWithComponents: aFirstAccountOrPortfolio and: aSecondAccountOrPortfolio

	| portfolio |
	
	portfolio := self portfolioWithComponent: aFirstAccountOrPortfolio.
	
	portfolio add: aSecondAccountOrPortfolio.
	
	^portfolio! !


!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:34:10'!
test01EmptyPortfolioHasBalanceZero

	| portfolio |
	
	portfolio := Portfolio new.
	
	self assert: 0 equals: portfolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:11:52'!
test02PortfolioWithAnAccountHasSameBalanceAsAccount

	| portfolio account |
	
	account := self accountWithDepositOf100.

	portfolio := self portfolioWithComponent: account.
	
	self assert: account balance equals: portfolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:12:47'!
test03PortfolioWithMultipleAccountsHasSameBalanceAsSumOfAccountBalances

	| portfolio account1 account2 |
	
	account1 := self accountWithDepositOf100.
	
	account2 := self accountWithDepositOf25.
	
	
	portfolio := self portfolioWithComponents: account1 and: account2.

	
	self assert: (account1 balance + account2 balance) equals: portfolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:34:21'!
test04PortfolioWithPortfolioHasSameBalanceAsInnerPortfolio

	| portfolioOuter portfolioInner account |
	
	account := self accountWithDepositOf100.
	
	
	portfolioInner := self portfolioWithComponent: account.
	
	portfolioOuter := self portfolioWithComponent: portfolioInner.

	
	self assert: portfolioInner balance equals: portfolioOuter balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:34:28'!
test05PortfolioWithMultiplePortfoliosHasSameBalanceAsSumOfInnerPortfolioBalances

	| portfolioOuter portfolioInner1 portfolioInner2 account1 account2 |
	
	account1 := self accountWithDepositOf100.
	
	account2 := self accountWithDepositOf175.
	
	
	portfolioInner1 := self portfolioWithComponent: account1.
	
	portfolioInner2 := self portfolioWithComponent: account2.
	
	
	portfolioOuter := self portfolioWithComponents: portfolioInner1 and: portfolioInner2.
	
	self assert: (portfolioInner1 balance + portfolioInner2 balance) equals: portfolioOuter balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 01:21:25'!
test06EmptyPortfolioReturnsFalseIfAskedIfItHasAnyTransaction

	| portfolio transaction |
	
	portfolio := Portfolio new.
	
	
	transaction := Deposit register: 100 on: ReceptiveAccount new.
	
	
	self assert: false equals: (portfolio hasRegistered: transaction).! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:52:08'!
test07TransactionIsRegisteredInPortfolioIfItsRegisteredInAnyOfItsAccounts

	| portfolio transaction account|
	

	account := ReceptiveAccount new.
	transaction := Deposit register: 100 on: account.
	
	portfolio := self portfolioWithComponent: account.
	
	
	self assert: true equals: (portfolio hasRegistered: transaction).! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:53:04'!
test08TransactionIsRegisteredInPortfolioIfItsRegisteredInAnyOfItsPortfolios

	| transaction account portfolioInner portfolioOuter |
	
	
	account := ReceptiveAccount new.
	transaction := Deposit register: 100 on: account.
	
	portfolioInner := self portfolioWithComponent: account.
	
	portfolioOuter := self portfolioWithComponent: portfolioInner.
	
	
	self assert: true equals: (portfolioOuter hasRegistered: transaction).! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/16/2023 19:28:10'!
test09EmptyPortfolioHasNoTransactions

	| portfolio |
	
	portfolio := Portfolio new.
	
	self assert: OrderedCollection new equals: portfolio transactions! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:18:06'!
test10PortfolioTransactionsListAllTransactionsInItsAccounts

	| portfolio account |
	

	account := self accountWithDepositOf100AndWithdrawalOf25. 
	
	portfolio := self portfolioWithComponent: account.
	
	
	self assert: account transactions equals: portfolio transactions! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:55:08'!
test11PortfolioTransactionsListAllTransactionsInItsPortfolios

	| account portfolioInner portfolioOuter |
	

	account := self accountWithDepositOf100AndWithdrawalOf25. 
	
	portfolioInner := self portfolioWithComponent: account.
	
	portfolioOuter := self portfolioWithComponent: portfolioInner.
	
	
	self assert: portfolioInner transactions equals: portfolioOuter transactions! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:40:30'!
test12AddingAccountAlreadyInPortfolioRaisesError

	| account portfolio |
	
	
	account := ReceptiveAccount new.
	
	portfolio := self portfolioWithComponent: account.

	self assertPortfolio: portfolio withOneComponent: account failsToAdd: account! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:40:55'!
test13AddingAccountAlreadyInAnInternalPortfolioRaisesError

	| account portfolioInner portfolioOuter |
	

	account := ReceptiveAccount new.
	
	portfolioInner := self portfolioWithComponent: account.
	
	portfolioOuter := self portfolioWithComponent: portfolioInner.
	
	self assertPortfolio: portfolioOuter withOneComponent: portfolioInner failsToAdd: account! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:42:34'!
test14AddingAccountAlreadyInAnOuterPortfolioRaisesError

	| account portfolioInner portfolioOuter |
	
	
	account := ReceptiveAccount new.
	
	portfolioInner := Portfolio new.
	
	portfolioOuter := self portfolioWithComponents: account and: portfolioInner.
	
	self assertEmptyPortfolio: portfolioInner failsToAdd: account! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:21:52'!
test15AddingRepeatedEmptyPortfolioRaisesError

	| portfolioInner portfolioOuter |
	

	portfolioInner := Portfolio new.
	
	portfolioOuter := self portfolioWithComponent: portfolioInner.
	
	self assertPortfolio: portfolioOuter withOneComponent: portfolioInner failsToAdd: portfolioInner! !

!PortfolioTest methodsFor: 'tests' stamp: 'MC 5/20/2023 17:45:40'!
test16AddingPortfolioWithRepeatedComponentsRaisesError

	| portfolioInner portfolioOuter account |
	
	
	account := ReceptiveAccount new.
	
	portfolioInner := self portfolioWithComponent: account.
	
	portfolioOuter := self portfolioWithComponent: account.
	
	self assertPortfolio: portfolioOuter withOneComponent: account failsToAdd: portfolioInner! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/17/2021 17:29:53'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'MC 5/16/2023 19:12:11'!
affectBalance: aBalanceValue 

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'MC 5/15/2023 20:26:52'!
affectBalance: aBalanceValue 

	^aBalanceValue + value! !

!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'MC 5/15/2023 20:26:37'!
affectBalance: aBalanceValue

	^ aBalanceValue - value! !

!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #BankClientBaseComponents category: 'Portfolio-Ejercicio'!
Object subclass: #BankClientBaseComponents
	instanceVariableNames: 'parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!BankClientBaseComponents methodsFor: 'add parent - private' stamp: 'MC 5/19/2023 15:35:49'!
addParent: aPortfolio

	parents addLast: aPortfolio.! !


!BankClientBaseComponents methodsFor: 'balance' stamp: 'MC 5/16/2023 20:11:51'!
balance

	self subclassResponsibility ! !


!BankClientBaseComponents methodsFor: 'transactions' stamp: 'MC 5/16/2023 20:11:44'!
transactions

	self subclassResponsibility ! !


!BankClientBaseComponents methodsFor: 'testing - private' stamp: 'MC 5/19/2023 15:21:38'!
repeatsComponent: clientBaseComponent

	self subclassResponsibility ! !


!BankClientBaseComponents methodsFor: 'testing' stamp: 'MC 5/16/2023 20:11:31'!
hasRegistered: aTransaction

	self subclassResponsibility ! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
BankClientBaseComponents subclass: #Portfolio
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'accessing' stamp: 'MC 5/19/2023 14:57:55'!
contents

	^ contents copy! !


!Portfolio methodsFor: 'adding' stamp: 'MC 5/19/2023 17:15:08'!
add: aClientBaseComponent


	self assertCanAdd: aClientBaseComponent.
	
	contents add: aClientBaseComponent.
	
	aClientBaseComponent addParent: self.! !


!Portfolio methodsFor: 'balance' stamp: 'MC 5/19/2023 14:38:36'!
balance

	^ contents sum:[:accountOrPortfolio | accountOrPortfolio balance] ifEmpty: [0].
	
	! !


!Portfolio methodsFor: 'testing' stamp: 'MC 5/21/2023 18:09:22'!
hasRegistered: aTransaction

	^ contents anySatisfy: [:component | component hasRegistered: aTransaction]! !


!Portfolio methodsFor: 'initialization' stamp: 'MC 5/19/2023 15:33:53'!
initialize 

	contents := OrderedCollection new.
	
	parents := OrderedCollection new.! !


!Portfolio methodsFor: 'transactions' stamp: 'MC 5/20/2023 18:18:31'!
transactions

	| transactions |
	
	transactions := OrderedCollection new.
	
	self addTransactionsTo: transactions.
	
	^ transactions.
	! !


!Portfolio methodsFor: 'transactions - private' stamp: 'MC 5/20/2023 18:14:54'!
addTransactionsTo: aCollectionOfTransactions

	contents do: [:aClientBaseComponent | aClientBaseComponent addTransactionsTo: aCollectionOfTransactions]! !


!Portfolio methodsFor: 'assertions - private' stamp: 'MC 5/19/2023 17:37:48'!
assertCanAdd: aClientBaseComponent

	| rootPortfolios newComponents |
	
	
	rootPortfolios := OrderedCollection new.
	
	self storeRootPortfoliosIn: rootPortfolios.
	

	newComponents := OrderedCollection new.
	
	aClientBaseComponent storeComponentsIn: newComponents.
	
	self assertComponents: newComponents areNotRepeatedInAny: rootPortfolios.
	
	
	! !

!Portfolio methodsFor: 'assertions - private' stamp: 'MC 5/19/2023 17:37:21'!
assertComponent: newComponent isNotRepeatedInAny: rootPortfoliosCollection

	(self component: newComponent isNotRepeatedInAny: rootPortfoliosCollection)
		ifTrue: [self error: self class portfolioCannotHaveRepeatedComponentsErrorDescription]
! !

!Portfolio methodsFor: 'assertions - private' stamp: 'MC 5/19/2023 17:34:27'!
assertComponents: clientBaseComponentsCollection areNotRepeatedInAny: rootPortfoliosCollection


	clientBaseComponentsCollection do: [:newComponent | self assertComponent: newComponent isNotRepeatedInAny: rootPortfoliosCollection].
! !


!Portfolio methodsFor: 'composition - private' stamp: 'MC 5/19/2023 16:41:00'!
storeComponentsIn: rootPortfoliosCollection

	rootPortfoliosCollection addLast: self.
	
	contents do: [:component | component storeComponentsIn: rootPortfoliosCollection].
	! !


!Portfolio methodsFor: 'testing - private' stamp: 'MC 5/19/2023 17:36:41'!
component: newComponent isNotRepeatedInAny: rootPortfoliosCollection

	^ rootPortfoliosCollection anySatisfy: [:rootPortfolio | rootPortfolio repeatsComponent: newComponent] 
	
! !

!Portfolio methodsFor: 'testing - private' stamp: 'MC 5/19/2023 17:07:39'!
contentsRepeatComponent: clientBaseComponent

	 ^contents anySatisfy: [:component | component repeatsComponent: clientBaseComponent]! !

!Portfolio methodsFor: 'testing - private' stamp: 'MC 5/19/2023 17:08:07'!
repeatsComponent: clientBaseComponent

	 ^(self = clientBaseComponent) or: (self contentsRepeatComponent: clientBaseComponent).! !


!Portfolio methodsFor: 'root - private' stamp: 'MC 5/19/2023 17:06:28'!
nonRootStoreRootPortfoliosIn: rootPortfoliosCollection

	parents do: [:aPortfolio | aPortfolio storeRootPortfoliosIn: rootPortfoliosCollection]
	! !

!Portfolio methodsFor: 'root - private' stamp: 'MC 5/19/2023 17:06:22'!
storeRootPortfoliosIn: rootPortfoliosCollection

	parents isEmpty ifTrue: [rootPortfoliosCollection addLast: self]
					ifFalse: [self nonRootStoreRootPortfoliosIn: rootPortfoliosCollection].
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'MC 5/19/2023 15:05:13'!
portfolioCannotHaveRepeatedComponentsErrorDescription

	^ 'Portfolio cannot have same element multiple times.'! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
BankClientBaseComponents subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'MC 5/19/2023 15:49:00'!
initialize

	transactions := OrderedCollection new.
	
	parents := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'MC 5/15/2023 20:26:08'!
balance

	
	| balance |
	balance := 0.
	
	transactions do: [ :aTransaction |  
		balance := aTransaction affectBalance: balance.
		].
	
	^balance! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'composition - private' stamp: 'MC 5/19/2023 16:41:18'!
storeComponentsIn: rootPortfoliosCollection

	rootPortfoliosCollection addLast: self.! !


!ReceptiveAccount methodsFor: 'transactions - private' stamp: 'MC 5/20/2023 18:10:43'!
addTransactionsTo: aCollectionOfTransactions

	transactions do: [:transaction | aCollectionOfTransactions addLast: transaction].! !


!ReceptiveAccount methodsFor: 'testing - private' stamp: 'MC 5/19/2023 15:21:29'!
repeatsComponent: clientBaseComponent

	^ self = clientBaseComponent! !
