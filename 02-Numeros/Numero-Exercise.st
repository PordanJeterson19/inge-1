!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'MC 4/16/2023 19:18:53'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de número inválido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'ms 4/17/2023 21:55:08'!
* aMultiplier 

	^ aMultiplier multiplyEntero: self
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ms 4/17/2023 21:55:12'!
+ anAdder 

	^anAdder addToEntero: self


	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MC 4/18/2023 01:07:04'!
- aSubtrahend 

	^aSubtrahend  subtractFromEntero: self 
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ms 4/17/2023 21:33:12'!
/ aDivisor 

	^ aDivisor divideEntero: self
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MC 4/18/2023 18:24:13'!
// aDivisor 
	
	^Entero with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'f 4/19/2023 16:15:33'!
fibonacci

	self subclassResponsibility ! !

!Entero methodsFor: 'arithmetic operations' stamp: 'MC 4/18/2023 18:23:49'!
greatestCommonDivisorWith: anEntero 
	
	^Entero with: (value gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 20:09'!
initalizeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'NR 9/23/2018 22:17:55'!
isNegative
	
	^value < 0! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:14'!
isOne
	
	^value = 1! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:12'!
isZero
	
	^value = 0! !


!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'MC 4/18/2023 18:24:06'!
addToEntero: unEntero
	^Entero with: value + unEntero integerValue

	

	! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'ms 4/17/2023 21:06:36'!
addToFraccion: unaFraccion
	
	^Fraccion with: ((self * unaFraccion denominator )+ unaFraccion numerator) over: unaFraccion denominator

	

	! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:20:46'!
divideEntero: enteroADividir 


	^Fraccion with: enteroADividir over: self

	! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'ms 4/17/2023 21:33:01'!
divideFraccion: fraccionADividir 

	 ^Fraccion  with:(fraccionADividir numerator ) over: (self * fraccionADividir denominator )
	
	! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'MC 4/18/2023 18:23:08'!
multiplyEntero: unEnteroAMultiplicar 

	^Entero with: value * unEnteroAMultiplicar integerValue

	! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'ms 4/17/2023 21:20:24'!
multiplyFraccion: unaFraccionAMultiplicar 

	^Fraccion with:(unaFraccionAMultiplicar numerator * self ) over: (unaFraccionAMultiplicar denominator )

	! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'MC 4/18/2023 18:23:29'!
subtractFromEntero: enteroDelQueSeResta

	^ Entero with: enteroDelQueSeResta integerValue - self integerValue.
	
! !

!Entero methodsFor: 'arithmetic - auxiliar' stamp: 'MC 4/18/2023 01:07:21'!
subtractFromFraccion: fraccionDeLaQueSeResta

	^ Fraccion with:(fraccionDeLaQueSeResta numerator - (self * (fraccionDeLaQueSeResta denominator))) over: fraccionDeLaQueSeResta denominator ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no está definido aquí para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'f 4/18/2023 22:50:59'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	(aValue < 0) ifTrue: [^EnteroNegativo with: aValue].
	(aValue<2) ifTrue: [ ^EnterosCeroYUno with: aValue ].
	(aValue>=2) ifTrue: [  ^EnterosMayoresAUno with: aValue ].
! !


!classDefinition: #EnteroNegativo category: 'Numero-Exercise'!
Entero subclass: #EnteroNegativo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroNegativo methodsFor: 'initialization' stamp: 'MC 4/18/2023 17:57:56'!
initalizeWith: aValue 
	
	value := aValue! !


!EnteroNegativo methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:52:11'!
assertThatDivisorIsNotCero: aDividend

	^aDividend  shouldDividendBeCeroReturnCero: self.! !

!EnteroNegativo methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:55:39'!
negateDividendAndDivisor: aDividend

 	^ (aDividend negated) / (self negated) 
	
! !

!EnteroNegativo methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:54:40'!
shouldDividendBeCeroReturnCero: aDivisor

	^aDivisor shouldDivisorBeNegativeNegateFraccion: self! !

!EnteroNegativo methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:55:39'!
shouldDivisorBeNegativeNegateFraccion: aDividend
	
	
	^ self negateDividendAndDivisor: aDividend

! !

!EnteroNegativo methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:53:20'!
shouldDivisorBeUnoReturnDividend: numerator

	^Fraccion new initializeWith: numerator over: self! !


!EnteroNegativo methodsFor: 'arithmetic operations' stamp: 'MC 4/18/2023 18:26:46'!
fibonacci

	self error: Entero negativeFibonacciErrorDescription ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroNegativo class' category: 'Numero-Exercise'!
EnteroNegativo class
	instanceVariableNames: ''!

!EnteroNegativo class methodsFor: 'as yet unclassified' stamp: 'MC 4/18/2023 18:21:03'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	(aValue<0) ifFalse: [  self error: 'aValue debe ser negativo' ].
	
	
	^self new initalizeWith: aValue! !


!classDefinition: #EnterosCeroYUno category: 'Numero-Exercise'!
Entero subclass: #EnterosCeroYUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnterosCeroYUno methodsFor: 'as yet unclassified' stamp: 'MC 4/18/2023 18:40:11'!
fibonacci

	| one |
	one := Entero with: 1.
	
	^one! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnterosCeroYUno class' category: 'Numero-Exercise'!
EnterosCeroYUno class
	instanceVariableNames: ''!

!EnterosCeroYUno class methodsFor: 'as yet unclassified' stamp: 'f 4/18/2023 23:15:59'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	(aValue<0) ifTrue: [  self error: 'aValue debe ser cero o uno' ].
	(aValue>1) ifTrue: [  self error: 'aValue debe ser cero o uno' ].
	(aValue = 0) ifTrue: [  ^Cero with: aValue ].
	(aValue = 1) ifTrue: [  ^Uno with: aValue ].
! !


!classDefinition: #Cero category: 'Numero-Exercise'!
EnterosCeroYUno subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Cero methodsFor: 'initialization' stamp: 'f 4/18/2023 23:16:39'!
initalizeWith: aValue 
	
	value := aValue! !


!Cero methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:48:22'!
assertThatDivisorIsNotCero: aDividend

	self error: Numero canNotDivideByZeroErrorDescription ! !

!Cero methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:52:11'!
shouldDividendBeCeroReturnCero: aDivisor

	^self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cero class' category: 'Numero-Exercise'!
Cero class
	instanceVariableNames: ''!

!Cero class methodsFor: 'as yet unclassified' stamp: 'f 4/18/2023 23:18:01'!
with: aValue 
	
	(aValue = 0) ifFalse: [  self error: 'aValue debe ser cero' ].
	
	
	^self new initalizeWith: aValue! !


!classDefinition: #Uno category: 'Numero-Exercise'!
EnterosCeroYUno subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'initialization' stamp: 'f 4/18/2023 23:16:45'!
initalizeWith: aValue 
	
	value := aValue! !


!Uno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:52:11'!
assertThatDivisorIsNotCero: aDividend

	^aDividend  shouldDividendBeCeroReturnCero: self.! !

!Uno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:54:40'!
shouldDividendBeCeroReturnCero: aDivisor

	^aDivisor shouldDivisorBeNegativeNegateFraccion: self! !

!Uno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:54:40'!
shouldDivisorBeNegativeNegateFraccion: aDividend

	| denominator greatestCommonDivisor numerator |
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: self. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.
	
	^denominator shouldDivisorBeUnoReturnDividend: numerator
! !

!Uno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:53:20'!
shouldDivisorBeUnoReturnDividend: numerator
	
	^numerator .


	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Uno class' category: 'Numero-Exercise'!
Uno class
	instanceVariableNames: ''!

!Uno class methodsFor: 'as yet unclassified' stamp: 'f 4/18/2023 23:18:22'!
with: aValue 
	
	(aValue = 1) ifFalse: [  self error: 'aValue debe ser uno' ].
	
	
	^self new initalizeWith: aValue! !


!classDefinition: #EnterosMayoresAUno category: 'Numero-Exercise'!
Entero subclass: #EnterosMayoresAUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnterosMayoresAUno methodsFor: 'arithmetic operations' stamp: 'MC 4/18/2023 18:40:44'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.

	^ (self - one) fibonacci + (self - two) fibonacci! !


!EnterosMayoresAUno methodsFor: 'initialization' stamp: 'MC 4/18/2023 18:38:55'!
initalizeWith: aValue 
	
	value := aValue! !


!EnterosMayoresAUno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:52:11'!
assertThatDivisorIsNotCero: aDividend

	^aDividend  shouldDividendBeCeroReturnCero: self.! !

!EnterosMayoresAUno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:54:40'!
shouldDividendBeCeroReturnCero: aDivisor

	^aDivisor shouldDivisorBeNegativeNegateFraccion: self! !

!EnterosMayoresAUno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:54:40'!
shouldDivisorBeNegativeNegateFraccion: aDividend

	| denominator greatestCommonDivisor numerator |
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: self. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.
	
	^denominator shouldDivisorBeUnoReturnDividend: numerator
	! !

!EnterosMayoresAUno methodsFor: 'Fraccion creation' stamp: 'f 4/19/2023 16:53:20'!
shouldDivisorBeUnoReturnDividend: numerator

	^Fraccion new initializeWith: numerator over: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnterosMayoresAUno class' category: 'Numero-Exercise'!
EnterosMayoresAUno class
	instanceVariableNames: ''!

!EnterosMayoresAUno class methodsFor: 'as yet unclassified' stamp: 'MC 4/18/2023 18:45:57'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	(aValue<2) ifTrue: [  self error: 'aValue debe ser mayor a uno' ].
	
	^self new initalizeWith: aValue! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ms 4/17/2023 21:55:28'!
* aMultiplier 

	^ aMultiplier multiplyFraccion: self
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ms 4/17/2023 21:55:32'!
+ anAdder 
	
	^anAdder addToFraccion: self
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'MC 4/18/2023 01:06:20'!
- aSubtrahend 

	^ aSubtrahend  subtractFromFraccion: self
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'ms 4/17/2023 21:55:47'!
/ aDivisor 

	^ aDivisor divideFraccion: self
	! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:27:21'!
addToEntero: unEntero

	^ unEntero addToFraccion: self! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'ms 4/17/2023 21:04:55'!
addToFraccion: unaFraccion
	
	| newNumerator newDenominator |
	
	newNumerator := (unaFraccion numerator * self denominator) + (unaFraccion denominator * self numerator).
	newDenominator := unaFraccion denominator * self denominator.
	
	^newNumerator / newDenominator 
! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'ms 4/17/2023 21:35:04'!
divideEntero: enteroADividir

	^Fraccion with:(enteroADividir * self denominator) over:(self numerator)! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:19:47'!
divideFraccion: fraccionADividir 

	^Fraccion with: (fraccionADividir numerator * self denominator) over: (fraccionADividir denominator * self numerator)
! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:26:14'!
multiplyEntero: enteroAMultiplicar 
	
	^enteroAMultiplicar multiplyFraccion: self

	! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:19:27'!
multiplyFraccion: fraccionAMultiplicar

	^( self numerator * fraccionAMultiplicar numerator) / (self denominator * fraccionAMultiplicar denominator)
! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:32:09'!
subtractFromEntero: enteroDelQueSeResta

	^ (enteroDelQueSeResta subtractFromFraccion: self) negated ! !

!Fraccion methodsFor: 'arithmetic - auxiliar' stamp: 'f 4/19/2023 16:20:29'!
subtractFromFraccion: fraccionDeLaQueSeResta

	| newNumerator newDenominator |
	
	newNumerator := (fraccionDeLaQueSeResta numerator * self denominator) - (fraccionDeLaQueSeResta denominator * self numerator).
	newDenominator := fraccionDeLaQueSeResta denominator * self denominator.
	
	^newNumerator / newDenominator 

! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'f 4/19/2023 16:48:22'!
with: aDividend over: aDivisor
	
	^aDivisor assertThatDivisorIsNotCero: aDividend.
	! !
